<?php
/**
 * Glasses Shop ©
 * Copyright © 2012 Georgi Ivanov <gopa_bg@abv.bg>
 *
 * LICENSE
 *
 * A copy of this license is bundled with this package in the file LICENSE.txt.
 *
 * Copyright © Glasses Shop
 *
 * Platform that uses this site is protected by copyright.
 * It is provided solely for the use of this site and all its copying,
 * processing or use of parts thereof is prohibited and pursued by law.
 *
 * @author Georgi Ivanov <gopa_bg@abv.bg>
 * @category GZend
 * @package GZend Entity
 * @copyright GAZE-ND
 */
 
/**
 * Abstract Class for Value Objects
 *
 * @author Georgi Ivanov <gopa_bg@abv.bg>
 * @category GZend
 * @package GZend Entity
 * @copyright GAZE-ND
 */
abstract class GZend_Entity_AbstractEntity
{
    
    /**
     * Proxies to {@link populate}
     *
     * @param array $options
     * @return GZend_Entity_AbstractEntity
     */
    public function setOptions(array $options)
    {
        $this->populate($options);
        return $this;
    }
    
    /**
     * Populates entity values
     *
     * @param array $options
     * @return GZend_Entity_AbstractEntity
     */
    public function populate(array $options)
    {
        $dashToCamelFilter = new Zend_Filter_Word_DashToCamelCase();
        foreach ($options as $option => $value) {
            $setter = 'set' . ucfirst($dashToCamelFilter->filter($option));
            if (method_exists($this, $setter)) {
                $this->{$setter}($value);
            }
        }
        return $this;
    }
    
    /**
     * Fix for PHP 5.2 missing function lcfirst()
     *
     * @param string $str
     * @return string
     */
    protected function lowerCaseFirst($str)
    {
        if(function_exists('lcfirst')) {
            return lcfirst($str);
        } else {
            $str[0] = strtolower($str[0]);
            return $str;
        }
    }
    
    /**
     * Get entity values
     *
     * @return array
     */
    public function toArray()
    {
        $data = array();
    
        $methods = get_class_methods($this);
        $methodPattern = '@^set([a-zA-Z0-9]+)$@';
    
        foreach ($methods as $method) {
            if (preg_match($methodPattern, $method, $matches)) {
                $methodShortName = $matches[1];
                $getter = 'get' . $methodShortName;
    
                if (method_exists($this, $getter)) {
                    $value = $this->{$getter}();
                    $data[$this->lowerCaseFirst($methodShortName)] = $value;
                }
            }
        }
    
        /* @var $data array */
        return $data;
    }
    
    
    /**
     * Uppercase first letter in every key of an array.
     *
     * Could wrap pairs in a subarray if $wrap is defined.
     *
     * @param array $data
     * @param string $wrap
     * @return array
     */
    protected function keyUpperCaseFirstLetter(array $data, $wrap = null)
    {
        $result = array();
        foreach ($data as $key => $value) {
            $result[ucfirst($key)] = $value;
        }
    
        return !empty($wrap) ? array($wrap => $result) : $result;
    }
}