<?php
/**
 * Created by PhpStorm.
 * User: gopa
 * Date: 5/14/14
 * Time: 5:16 PM
 */

/**
 * Image processing class (from TomatoCMS).
 *
 * Factory for image processing.
 *
 * @category GZend
 * @package GZend_Image
 * @subpackage Image
 * @copyright Copyright (c) 2010 Sasquatch MC
 */
class GZend_Image_ProcessorFactory
{
    /**
     * Factory method.
     *
     * Get instance of a specific image proccessing adapter.
     * Implements the {@link http://en.wikipedia.org/wiki/Factory_method_pattern Factory Method} Design Pattern.
     *
     * @param string $adapter Name of the image adapter
     * @return GZend_Image_AbstractProcessor
     * @throws GZend_Image_ProcessorNotFoundException
     */
    public function factory($adapter)
    {
        $adapter = strtolower($adapter);
        switch ($adapter) {
            case 'gd':
                return new GZend_Image_GDProcessor();
            break;

            case 'imagemagick':
                return new GZend_Image_ImageMagickProcessor();
            break;
            default:
                throw new GZend_Image_ProcessorNotFoundException();
        }
    }
}