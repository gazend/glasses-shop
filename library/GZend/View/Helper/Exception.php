<?php
/**
 * Glasses Shop ©
 * Copyright © 2012 Georgi Ivanov <gopa_bg@abv.bg>
 *
 * LICENSE
 *
 * A copy of this license is bundled with this package in the file LICENSE.txt.
 *
 * Copyright © Glasses Shop
 *
 * Platform that uses this site is protected by copyright.
 * It is provided solely for the use of this site and all its copying,
 * processing or use of parts thereof is prohibited and pursued by law.
 *
 * @author Georgi Ivanov <gopa_bg@abv.bg>
 * @category GZend
 * @package GZend Helpers
 * @copyright GAZE-ND
 */

/**
 * Helpers Exception
 *
 * @author Georgi Ivanov <gopa_bg@abv.bg>
 * @category GZend
 * @package GZend Helpers
 * @copyright GAZE-ND
 */
class GZend_View_Helper_Exception extends Zend_Exception
{
    
}