<?php


class GZend_Di_ServiceManager
{

    private $services = [];

    public function __construct($config = null)
    {
        if (null !== $config) {
            if (is_array($config)) {
                $this->registerServices($config);
            } else {
                $this->loadFromFile($config);
            }
        }
    }

    /**
     * Register single service
     *
     * @param string $id
     * @param mixed $service
     * @return $this
     */
    public function registerService($id, $service)
    {
        $this->services[$id] = $service;

        return $this;
    }

    /**
     * Get service
     *
     * @param string $id
     * @return mixed
     * @throws GZend_Di_ServiceNotFoundException When service is not registered
     */
    public function get($id)
    {
        if (isset($this->services[$id])) {
            if (is_callable($this->services[$id])) {
                return $this->services[$id]($this);
            } else {
                return $this->services[$id];
            }

        }
        throw new GZend_Di_ServiceNotFoundException("Service with '{$id}' not found!");
    }

    /**
     * Register multiple services
     *
     * @param array $services
     * @return $this
     */
    public function registerServices(array $services)
    {
        foreach ($services as $serviceId => $service) {
            $this->registerService($serviceId, $service);
        }
        return $this;
    }

    /**
     * Load configuration from file
     *
     * @param string $file
     * @return $this
     * @throws GZend_Di_ConfigNotFoundException When config file doesn't exist
     */
    public function loadFromFile($file)
    {
        if (!file_exists($file)) {
            throw new GZend_Di_ConfigNotFoundException("File '{$file}' not found!");
        }
        $config = include ($file);
        $this->registerServices($config);
        return $this;
    }
}

