<?php
/**
 * Created by PhpStorm.
 * User: gopa
 * Date: 5/14/14
 * Time: 4:39 PM
 */

/**
 * Image resize filter.
 *
 * @category GZend
 * @package GZend_Filter
 * @subpackage Image
 * @copyright Copyright (c) 2010 Sasquatch MC
 * @uses Zend_View_Helper_Abstract
 */
class GZend_Filter_Image_Thumb extends GZend_Filter_Image_Resize
{

    /**
     * @var string
     */
    protected $_thumbFolder = 'thumbs';

    /**
     * Constructor.
     *
     * Set options.
     *
     * @param array|null $options
     * @return \GZend_Filter_Image_Thumb
     */
    public function __construct($options = null)
    {
        parent::__construct($options);
        if (isset($options['thumbFolder'])) {
            $this->setThumbFolder($options['thumbFolder']);
        }
    }

    /**
     * Set GZend_Filter_Image_Thumb::$_thumbFolder
     *
     * @param string $thumbFolder
     * @return GZend_Filter_Image_Thumb
     */
    public function setThumbFolder($thumbFolder)
    {
        $this->_thumbFolder = (null === $thumbFolder) ? null : (string) $thumbFolder;
        return $this;
    }

    /**
     * Get GZend_Filter_Image_Thumb::$_thumbFolder
     *
     * @return string
     */
    public function getThumbFolder()
    {
        return $this->_thumbFolder;
    }

    /**
     * Resize image
     *
     * @param string $value
     * @return string
     */
    public function filter($value)
    {
        $fileName = $value;
        $fileName = dirname($fileName) . '/'
            . $this->_thumbFolder . '/'
            . basename($fileName);

        $imageProccessor = GZend_Image_ProcessorFactory::factory($this->getAdapter());

        $imageProccessor->setFile($value)->resizeLimit($fileName, $this->getWidth(), $this->getHeight());
        return $value;
    }

}