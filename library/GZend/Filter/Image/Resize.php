<?php
/**
 * Created by PhpStorm.
 * User: gopa
 * Date: 5/14/14
 * Time: 4:23 PM
 */
/**
 * Image resize filter.
 *
 * @category GZend
 * @package GZend_Filter
 * @subpackage Image
 *
 */
class GZend_Filter_Image_Resize implements Zend_Filter_Interface
{
    /**
     * @var int
     */
    protected $_width = null;

    /**
     * @var int
     */
    protected $_height = null;

    /**
     * @var string
     */
    protected $_newFileName = null;

    /**
     * @var string
     */
    protected $_adapter = 'ImageMagick';

    /**
     * Constructor.
     *
     * Set options.
     *
     * @param array|null $options
     * @return GZend_Filter_Image_Resize
     */
    public function __construct($options = null)
    {
        if (isset($options['width'])) {
            $this->setWidth($options['width']);
        }

        if (isset($options['height'])) {
            $this->setHeight($options['height']);
        }

        if (isset($options['adapter'])) {
            $this->setAdapter($options['adapter']);
        }

        if (isset($options['newFileName'])) {
            $this->setNewFileName($options['newFileName']);
        }
    }

    /**
     * Set GZend_Filter_Image_Resize::$_width
     *
     * @param int $width
     * @return GZend_Filter_Image_Resize
     */
    public function setWidth($width = null)
    {
        $this->_width = (int) $width;
        return $this;
    }

    /**
     * Get GZend_Filter_Image_Resize::$_width
     *
     * @return int
     */
    public function getWidth()
    {
        return $this->_width;
    }

    /**
     * Set GZend_Filter_Image_Resize::$_height
     *
     * @param int $height
     * @return GZend_Filter_Image_Resize
     */
    public function setHeight($height = null)
    {
        $this->_height = (int) $height;
        return $this;
    }

    /**
     * Get GZend_Filter_Image_Resize::$_height
     *
     * @return int
     */
    public function getHeight()
    {
        return $this->_height;
    }

    /**
     * Set GZend_Filter_Image_Resize::$_newFileName
     *
     * @param string $newFileName
     * @return GZend_Filter_Image_Resize
     */
    public function setNewFileName($newFileName = null)
    {
        $this->_newFileName = (string) $newFileName;
        return $this;
    }

    /**
     * Get GZend_Filter_Image_Resize::$_newFileName
     *
     * @return string
     */
    public function getNewFileName()
    {
        return $this->_newFileName;
    }

    /**
     * Set GZend_Filter_Image_Resize::$_adapter
     *
     * @param string $adapter
     * @return GZend_Filter_Image_Resize
     */
    public function setAdapter($adapter)
    {
        $this->_adapter = (null === $adapter) ? null : (string) $adapter;
        return $this;
    }

    /**
     * Get GZend_Filter_Image_Resize::$_adapter
     *
     * @return string
     */
    public function getAdapter()
    {
        return $this->_adapter;
    }

    /**
     * Resize image
     *
     * @param string $value
     * @return string
     */
    public function filter($value)
    {
        if (!empty($this->_newFileName)) {
            $fileName = $this->_newFileName;
        } else {
            $fileName = $value;
        }

        $imageProccessor = GZend_Image_ProcessorFactory::factory($this->getAdapter());

        $imageProccessor->setFile($value)->resizeLimit($fileName, $this->getWidth(), $this->getHeight());
        return $value;
    }

}