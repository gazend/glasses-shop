<?php
/**
 * Created by PhpStorm.
 * User: gopa
 * Date: 4/30/14
 * Time: 2:15 PM
 */

class GZend_Filter_RenameSaveExtension implements Zend_Filter_Interface
{
    /**
     * @var string
     */
    protected $_name = null;

    /**
     * @var boolean
     */
    protected $_generateUniqueId = false;

    /**
     * @var boolean
     */
    protected $_prepend = false;

    /**
     * Constructor.
     *
     * Set options.
     *
     * @param array|string $options If is string - $options is the name
     */
    public function __construct($options)
    {
        if (is_string($options)) {
            $this->setName($options);
            return;
        } else
            if (is_array($options)) {
                if (!empty($options['name'])) {
                    $this->setName($options['name']);
                }
                if (!empty($options['prepend'])) {
                    $this->setPrepend($options['prepend']);
                }
                if (!empty($options['generateUniqueId'])) {
                    $this->setGenerateUniqueId($options['generateUniqueId']);
                }
            }
    }

    /**
     * Set Application_Filter_RenameSaveExtension::$_name
     *
     * @param string $name
     * @return GZend_Filter_RenameSaveExtension
     */
    public function setName($name = null)
    {
        $this->_name = (string) $name;
        return $this;
    }

    /**
     * Get Application_Filter_RenameSaveExtension::$_name
     *
     * @return string
     */
    public function getName()
    {
        return $this->_name;
    }

    /**
     * Set Application_Filter_RenameSaveExtension::$_generateUniqueId
     *
     * @param boolean $generateUniqueId
     * @return GZend_Filter_RenameSaveExtension
     */
    public function setGenerateUniqueId($generateUniqueId = false)
    {
        $this->_generateUniqueId = (boolean) $generateUniqueId;
        return $this;
    }

    /**
     * Get Application_Filter_RenameSaveExtension::$_generateUniqueId
     *
     * @return boolean
     */
    public function getGenerateUniqueId()
    {
        return $this->_generateUniqueId;
    }

    /**
     * Set Application_Filter_RenameSaveExtension::$_prepend
     *
     * @param boolean $prepend
     * @return GZend_Filter_RenameSaveExtension
     */
    public function setPrepend($prepend = true)
    {
        $this->_prepend = (null === $prepend) ? true : (boolean) $prepend;
        return $this;
    }

    /**
     * Get Application_Filter_RenameSaveExtension::$_prepend
     *
     * @return boolean
     */
    public function getPrepend()
    {
        return $this->_prepend;
    }

    /**
     * Rename file, but save the extension
     *
     * @param string $value
     * @return string
     */
    public function filter($value)
    {
        $fileInfo = pathinfo($value);
        $extension = $fileInfo['extension'];
        $dirname = $fileInfo['dirname'];
        $filename = $fileInfo['filename'];

        $newName = $dirname . DIRECTORY_SEPARATOR
            . ($this->getGenerateUniqueId() ? uniqid($this->getName(), true) : $this->getName())
            . '.' . strtolower($extension);

        if ($this->_prepend && $this->_generateUniqueId) {
            $newName = $dirname . DIRECTORY_SEPARATOR
                . uniqid($this->getName()) . '-'
                . $filename . '.' . strtolower($extension);
        }

        if (file_exists($value)) {
            rename($value, $newName);
        }

        return realpath($newName);
    }

}