<?php

require_once ('Zend/Controller/Action.php');

class GZend_Controller_AdminActions extends GZend_Controller_AbstractAction
{

    const BACKEND_LAYOUT = 'backend';

    /**
     * Constructor
     * 
     * @param Zend_Controller_Request_Abstract $request
     * @param Zend_Controller_Response_Abstract $response
     * @param array $invokeArgs
     */
    public function __construct(Zend_Controller_Request_Abstract $request, Zend_Controller_Response_Abstract $response, array $invokeArgs = array())
    {
        parent::__construct($request, $response, $invokeArgs);
        
        Zend_Layout::getMvcInstance()->setLayout(self::BACKEND_LAYOUT);
        
        $controller = $this->getRequest()->getControllerName();
        $action = $this->getRequest()->getActionName();
        $module = $this->getRequest()->getModuleName();
        
        $isDestinationCorrect = $controller == 'admin' && $action == 'login' &&
                 $module == 'default';
        
        if (! Zend_Auth::getInstance()->hasIdentity() && ! $isDestinationCorrect) {
            $this->_helper->redirector->gotoSimpleAndExit('login', 'admin', 
                    'default');
        } elseif (Zend_Auth::getInstance()->hasIdentity() && $isDestinationCorrect) {
            $this->_helper->redirector->gotoSimpleAndExit('index', 'admin', 'default');
        }
        
        
    }
}