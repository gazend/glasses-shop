<?php

abstract class GZend_Controller_AbstractAction extends Zend_Controller_Action
{

    /**
     * Service manager
     *
     * @var GZend_Di_ServiceManager
     */
    protected $serviceManager;

    /**
     * Constructor
     *
     * @param Zend_Controller_Request_Abstract $request
     * @param Zend_Controller_Response_Abstract $response
     * @param array $invokeArgs
     */
    public function __construct(Zend_Controller_Request_Abstract $request, Zend_Controller_Response_Abstract $response, array $invokeArgs = array())
    {
        parent::__construct($request, $response, $invokeArgs);
        $this->serviceManager = Zend_Registry::get('SERVICE_MANAGER');
    }

}