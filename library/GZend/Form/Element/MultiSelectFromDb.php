<?php
/**
 * Created by PhpStorm.
 * User: gopa
 * Date: 4/29/14
 * Time: 5:05 PM
 */

class GZend_Form_Element_MultiSelectFromDb extends Zend_Form_Element_Multiselect
{

    /**
     * SQL Query
     *
     * @var string|Zend_Db_Select
     */
    protected $_sql = null;

    /**
     * Sets SQL Query
     *
     * @param string|Zend_Db_Select $sql
     * @return GZend_Form_Element_MultiSelectFromDb
     */
    public function setSql($sql)
    {
        $this->_sql = $sql;
        return $this;
    }

    /**
     * Get the SQL Query
     *
     * @return string|Zend_Db_Select
     */
    public function getSql()
    {
        return $this->_sql;
    }

    /**
     * Prepend option value
     *
     * @param mixed $option
     * @param mixed $value
     * @return GZend_Form_Element_MultiSelectFromDb
     */
    public function prependMultiOption($option, $value)
    {
        $this->options = array_reverse($this->options, true);
        $this->addMultiOption($option, $value);
        $this->options = array_reverse($this->options, true);
        return $this;
    }

    /**
     * init options
     * @param  array  $defaultValue
     * @return void
     */
    public function init($defaultValue = array())
    {
        if (null !== $this->_sql) {
            $dbAdapter = Zend_Db_Table_Abstract::getDefaultAdapter();
            $this->setMultiOptions($defaultValue + $dbAdapter->fetchPairs($this->_sql));
        }
    }
}