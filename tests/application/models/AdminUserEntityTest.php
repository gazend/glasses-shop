<?php

class AdminUserEntityTest extends PHPUnit_Framework_TestCase
{
    
    protected $entity;  
    
    protected $testData = array(
            'id' => 3,
            'username' => 'piperkov',
            'password' => 'fakturka',
            'email' => 'ppirkov@abv.bg'
    );

    protected function setUp()
    {
        $this->entity = new Application_Model_AdminUser();
    }
    
    public function testPopulate()
    {
        $entity = $this->entity;
        
        $data = $this->testData;
        $entity->populate($data);
        $this->assertEquals($data['id'], $entity->getId());
        $this->assertEquals($data['username'], $entity->getUsername());
        $this->assertEquals($data['password'], $entity->getPassword());
        $this->assertEquals($data['email'], $entity->getEmail());
    }
    
    public function testToArray()
    {
        $entity = $this->entity; 
        
        $data = $this->testData;
        $entity->populate($data);
        $actual = $entity->toArray();
        $this->assertEquals($data, $actual);
    }
}