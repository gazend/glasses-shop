<?php
/**
 * Created by PhpStorm.
 * User: gopa
 * Date: 10/16/14
 * Time: 4:21 PM
 */

class ProductPropertiesRepositoryTest extends PHPUnit_Framework_TestCase {

    /**
     * @var Products_Model_ProductPropertiesRepository
     */
    private $repo;

    protected function setUp()
    {
        $this->repo = new Products_Model_ProductPropertiesRepository(
            new Products_Model_ProductPropertiesGateway()
        );
    }

    public function testGetPropsByProductId()
    {
        $productId = 10;
        $props = $this->repo->getByProductId($productId);
        $this->assertContainsOnlyInstancesOf('Products_Model_ProductProperty',
            $props);
        foreach ($props as $prop) {
            echo $prop->getProperty()->getName() . ' is ' . $prop->getValue()
                        . PHP_EOL;
        }
    }

}
 