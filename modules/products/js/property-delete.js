(function ($) {

    var propertyDelete = function (deleteUrl) {
        this.deleteUrl = deleteUrl;
    };

    propertyDelete.prototype.delete = function (propertyId) {
        if (confirm('Are you sure you want to delete this property?')) {
            $.get(this.deleteUrl, {id: propertyId}, function (response) {
                if (response.isValid) {
                    $('#property-' + propertyId).remove();
                }
            });
        }
    };

    window.PropertyDelete = propertyDelete;

}) (window.jQuery);
