(function ($) {

    var categoryDelete = function (deleteUrl) {
        this.deleteUrl = deleteUrl;
    };

    categoryDelete.prototype.delete = function (categoryId) {
        if (confirm('Are you sure you want to delete this category?')) {
            $.get(this.deleteUrl, {id: categoryId}, function (response) {
                if (response.isValid) {
                    $('#category-' + categoryId).remove();
                }
            });
        }
    };

    window.CategoryDelete = categoryDelete;

}) (window.jQuery);