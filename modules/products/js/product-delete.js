(function ($) {

    var productDelete = function (deleteUrl) {
        this.deleteUrl = deleteUrl;
    };

    productDelete.prototype.delete = function (productId) {
        if (confirm('Are you sure you want to delete this product?')) {
            $.get(this.deleteUrl, {id: productId}, function (response) {
                if (response.isValid) {
                    $('#product-' + productId).remove();
                }
            });
        }
    };

    window.ProductDelete = productDelete;

}) (window.jQuery);