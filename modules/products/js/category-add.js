(function ($) {

    categoryAdd.prototype.add = function () {
        if (confirm('Are you sure you want to add this category?')) {
            $.get( function (response) {
                if (response.isValid) {
                    $('#name').add();
                }
            });
        }
    };

    window.CategoryAdd = categoryAdd;

}) (window.jQuery);