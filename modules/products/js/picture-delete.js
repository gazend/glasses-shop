(function ($) {

    var pictureDelete = function (deleteUrl) {
        this.deleteUrl = deleteUrl;
    };

    pictureDelete.prototype.delete = function (productId) {
        if (confirm('Are you sure you want to delete this picture?')) {
            $.get(this.deleteUrl, {id: productId}, function (response) {
                if (response.isValid) {
                    $('#image-' + productId).remove();
                } else {
                    alert(response.error);
                }
            });
        }
    };

    window.PictureDelete = pictureDelete;

}) (window.jQuery);