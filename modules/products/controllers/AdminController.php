<?php
/**
 * Glasses Shop ©
 * Copyright © 2012 Georgi Ivanov <gopa_bg@abv.bg>
 *
 * LICENSE
 *
 * A copy of this license is bundled with this package in the file LICENSE.txt.
 *
 * Copyright © Glasses Shop
 *
 * Platform that uses this site is protected by copyright.
 * It is provided solely for the use of this site and all its copying,
 * processing or use of parts thereof is prohibited and pursued by law.
 *
 * @author Georgi Ivanov <gopa_bg@abv.bg>
 * @category Admin
 * @package Admin Controllers
 * @copyright GAZE-ND
 */

/**
 * Admin Controller
 *
 * @author Georgi Ivanov <gopa_bg@abv.bg>
 * @category Products
 * @package Products Controllers
 * @copyright GAZE-ND
 */

class Products_AdminController extends GZend_Controller_AdminActions
{

    public function categoriesAction()
    {
        /** @var Products_Model_CategoriesMapper $categoriesMapper */
        $categoriesMapper = $this->serviceManager->get('Products_Model_CategoriesMapper');

        $this->view->categories = $categoriesMapper->getAllCategories();
    }

    public function newCategoryAction()
    {
        if ($this->_request->isXmlHttpRequest()) {
            $this->_helper->layout()->disableLayout();
        }

        $form = new Products_Form_Category();
        $this->view->form = $form;

        $request = $this->getRequest();
        if ($request->isPost() && $form->isValid($request->getPost())) {
            $category = new Products_Model_Category($form->getValues());

            /** @var Products_Model_CategoriesMapper $categoriesMapper */
            $categoriesMapper = $this->serviceManager->get('Products_Model_CategoriesMapper');
            $categoriesMapper->insert($category);
            $this->view->success = true;

            if (!$this->_request->isXmlHttpRequest()) {
                $this->_helper->redirector->goToRouteAndExit([
                    'action' => 'categories',
                    'status' => 'updated'
                ]);
            } else {
                $this->categoriesAction();
                $this->_helper->json([
                    'isValid' => true,
                    'categoryId' => $category->getId(),
                    'categories' => $this->view->render('admin/categories-table.phtml')
                ]);
            }
        }


    }

    public function updateCategoryAction()
    {
        $form = new Products_Form_Category();
        $this->view->form = $form;

        $request = $this->getRequest();
        if ($request->isPost() && $form->isValid($request->getPost())) {
            $category = new Products_Model_Category($form->getValues());
            $category->setId($request->getPost('id'));

            /** @var Products_Model_CategoriesMapper $categoriesMapper */
            $categoriesMapper = $this->serviceManager->get('Products_Model_CategoriesMapper');
            $categoriesMapper->update($category);
            $this->_helper->json(['isValid' => true]);


        } else {
            $this->_helper->json(['isValid' => false]);
        }
    }

    public function deleteCategoryAction()
    {
        $categoryId = $this->getRequest()->getParam('id');

        /** @var Products_Model_CategoriesMapper $categoriesMapper */
        $categoriesMapper = $this->serviceManager->get('Products_Model_CategoriesMapper');
        $categoriesMapper->delete($categoryId);
        $this->_helper->json(['isValid' => true]);
    }

    /** Products related functions */

    public function productsAction()
    {
        $form = new Products_Form_SearchProduct();
        $this->view->searchProductForm = $form;

        $request = $this->getRequest();
        if ($form->isValid($request->getParams())) {
            $searchName = $this->_getParam('searchName');
            $priceFrom = $this->_getParam('searchPriceFrom');
            $priceTo = $this->_getParam('searchPriceTo');
            $quantFrom = $this->_getParam('searchQuantFrom');
            $quantTo = $this->_getParam('searchQuantTo');

            /** @var Products_Model_ProductsMapper $productsMapper */
            $productsMapper = $this->serviceManager->get('Products_Model_ProductsMapper');
            $this->view->products = $productsMapper->searchProducts($searchName, $priceFrom, $priceTo, $quantFrom, $quantTo);
        }
    }

    public function newProductAction()
    {
        /** @var Products_Form_Product $form */
        $form = $this->serviceManager->get('Products_Form_Product');
        $this->view->form = $form;

        $request = $this->getRequest();
        if ($request->isPost() && $form->isValid($request->getPost())) {
            /** @var  Products_Model_ProductCreation $productCreation */
            $productCreation = $this->serviceManager->get('Products_Model_ProductCreation');
            $product = $productCreation->insert($form);
            $this->view->success = true;
            $this->_helper->redirector->goToRouteAndExit([
                'action' => 'update-product',
                'product-id' => $product->getId(),
                'status' => 'added'
            ]);
        }
    }

    public function updateProductAction()
    {

        /** @var Products_Form_Product $productForm */
        $productForm = $this->serviceManager->get('Products_Form_Product');
        $this->view->productForm = $productForm;

        $picturesForm = new Products_Form_Pictures();
        $picturesForm->setAction($this->view->url(['action' => 'add-pictures']));
        $this->view->picturesForm = $picturesForm;

        $this->view->status = $this->_getParam('status');
        $productId = $this->_getParam('product-id');
        $this->view->productId = $productId;

        $productsRepository = $this->serviceManager->get('Products_Model_ProductsRepository');
        $product = $productsRepository->findById($productId);
        $this->view->product = $product;
        $productForm->populateFromEntity($product);

        $request = $this->getRequest();
        if ($request->isPost() && $productForm->isValid($request->getPost())) {
            /** @var  Products_Model_ProductModification $productModification */
            $productModification = $this->serviceManager->get('Products_Model_ProductModification');
            $productModification->update($productForm, $productId);

            $this->_helper->redirector->goToRouteAndExit([
                'action' => 'update-product',
                'product-id' => $product->getId(),
                'status' => 'updated'
            ]);
        }
    }


    public function resetPictureAction()
    {
        try {
            /** @var Products_Model_DeleteCoverPhoto $deletePhoto */
            $deletePhoto = $this->serviceManager->get('Products_Model_DeleteCoverPhoto');
            $deletePhoto->delete($this->_getParam('product-id'));
            $this->_helper->json(['isValid' => true]);
        } catch (Products_Model_ProductNotFoundException $e) {
            $this->_helper->json(['isValid' => false, 'error' => $e->getMessage()]);
        }

    }

    public function deleteProductAction()
    {
        $productId = $this->getRequest()->getParam('id');
        $productDeleter = $this->serviceManager->get('Products_Model_ProductDeleter');
        $productDeleter->delete($productId);
        $this->_helper->json(['isValid' => true]);

    }

    public function addPicturesAction()
    {
        $productId = $this->_getParam('product-id');
        $form = new Products_Form_Pictures();
        $this->view->form = $form;

        $request = $this->getRequest();
        if ($request->isPost() && $form->isValid($request->getPost())) {
            $pictureAdder = $this->serviceManager->get('Products_Model_PictureAdder');
            $pictureAdder->insert($form, $productId);
            $this->_helper->redirector->goToRouteAndExit([
                'action' => 'update-product',
                'product-id' => $productId,
                'status' => 'added'
            ]);
        }

    }

    public function deletePicturesAction()
    {
        $request = $this->getRequest();

        if ($request->isPost()) {

            $pictureIds = $this->_getParam('picture-id');
            $picturesDeleter = $this->serviceManager->get('Products_Model_PicturesDeleter');
            $picturesDeleter->delete($pictureIds);
            $this->_helper->json([
                'isValid' => true,
                'pictureIds' => $pictureIds
            ]);

        }
        $this->_helper->json(['isValid' => false]);
    }

    /**
     * @var Products_Model_PropertiesRepository $propertiesRepository
     */
    public function propertiesAction()
    {
        $propertiesRepository = $this->serviceManager->get('Products_Model_PropertiesRepository');
        $this->view->properties = $propertiesRepository->getAllProperties();
    }

    public function newPropertyAction()
    {

        if ($this->_request->isXmlHttpRequest()){
            $this->_helper->layout()->disableLayout();
        }

        $form = new Products_Form_Property();
        $this->view->form = $form;

        $request = $this->getRequest();
        if ($request->isPost() && $form->isValid($request->getPost()))
        {
            $property = new Products_Model_Property($form->getValues());
            /** @var Products_Model_PropertiesRepository $propertyRepository */
            $propertyRepository = $this->serviceManager->get('Products_Model_PropertiesRepository');
            $propertyRepository->insert($property);
            $this->view->success = true;

            if (!$this->_request->isXmlHttpRequest()) {
                $this->_helper->redirector->goToRouteAndExit([
                    'action' => 'properties',
                    'status' => 'updated'
                ]);
            } else {
                $this->propertiesAction();
                $this->_helper->json([
                    'isValid' => true,
                    'propertyId' => $property->getId(),
                    'properties' => $this->view->render('admin/properties-table.phtml')
                ]);
            }
        }
    }

    public function deletePropertyAction()
    {

        $id = $this->getRequest()->getParam('id');

        /** @var  Products_Model_PropertiesRepository $propertyRepository*/
        $propertyRepository = $this->serviceManager->get('Products_Model_PropertiesRepository');
        $propertyRepository->delete($id);
        $this->_helper->json(['isValid' => true]);
    }

}
