<?php
/**
 * Created by PhpStorm.
 * User: gopa
 * Date: 4/4/14
 * Time: 3:51 PM
 */

class Products_Form_Category extends Application_Form_AbstractBootstrapable
{
    public function init()
    {
        $this->setMethod('POST');

        $this->setDescription('Set category name');

        // Category name
        $this->addElement('text', 'name', array(
            'label' => 'Category Name:',
            'filters' => array('StringTrim'),
            'required' => true
        ));
    }

} 