<?php
/**
 * Created by PhpStorm.
 * User: gopa
 * Date: 6/11/14
 * Time: 12:02 PM
 */

class Products_Form_Pictures extends Zend_Form
{
    public function init()
    {
        $this->setMethod('POST');
        $this->setDescription('Upload Product Pictures:');

        $this->addElement('file', 'pictures', array(
            'label' => 'Upload Images',
            'isArray' => true,
            'multiFile' => 5,
            'destination' => ROOT_PATH . '/public/uploads/products',
            'filters' => array(
                new GZend_Filter_RenameSaveExtension([
                    'generateUniqueId' => true,
                    'name' => 'product_photo_'
                ]),
                new GZend_Filter_Image_Resize([
                    'width' => 400,
                    'height' => 400
                ]),
                new GZend_Filter_Image_Thumb([
                    'width' => 100,
                    'height' => 100
                ])
            ),
            'validators' => [
                ['Extension', false, array('jpg', 'jpeg', 'png', 'gif')],
            ],


        ));

        // submit button
        $this->addElement('submit', 'submit', array(
            'label' => 'Upload',
            'ignore' => true
        ));
    }
} 