<?php
/**
 * Created by PhpStorm.
 * User: gopa
 * Date: 4/28/14
 * Time: 6:29 PM
 */

class Products_Form_Product extends Application_Form_AbstractBootstrapable
{
    private $categoriesMapper;

    public function __construct(Products_Model_CategoriesMapper $categoriesMapper)
    {
        $this->categoriesMapper = $categoriesMapper;
        parent::__construct();
    }

    public function init()
    {
        $this->setMethod('POST');

        $this->setDescription('Set Product Properties');

        $this->addPrefixPath('GZend_Form_Element', 'GZend/Form/Element', 'element');

        //productName

        $this->addElement('text', 'name', array(
            'label' => 'Product name:',
            'filters' => array('StringTrim'),
            'required' => true,
            'class' => $this->defaultClass
        ));

        //productPrice

        $this->addElement('text', 'price', array(
            'label' => 'Product price:',
            'filters' => array('StringTrim'),
            'required' => true,
            'class' => $this->defaultClass
        ));

        $this->addElement('MultiSelectFromDb', 'categoryIds', array(
            'label' => 'Categories:',
            'required' => true,
            'sql' => $this->categoriesMapper->getSelectForAllCategories(),
            'class' => $this->defaultClass
        ));

        // products amount
        $this->addElement('text', 'amountAvailable', array(
            'label' => 'Amount Available:',
            'filters' => array('StringTrim', 'Digits'),
            'required' => true,
            'class' => $this->defaultClass
        ));

        $this->addElement('textarea', 'description', array(
            'label' => 'Product description:',
            'filters' => array('StringTrim'),
            'required' => true,
            'class' => $this->defaultClass
        ));


        //Product pictures
        $this->addElement('file', 'picture', array(
            'label' => 'Display picture:',
            'destination' => ROOT_PATH . '/public/uploads/products',
            'filters' => array(
                new GZend_Filter_RenameSaveExtension([
                    'generateUniqueId' => true,
                    'name' => 'cover_photo_'
                ]),
                new GZend_Filter_Image_Resize([
                    'width' => 400,
                    'height' => 400
                ]),
                new GZend_Filter_Image_Thumb([
                    'width' => 50,
                    'height' => 50
                ])
            ),
            'validators' => [
                ['Extension', false, array('jpg', 'jpeg', 'png', 'gif')],
            ],
            'class' => 'btn btn-default',
            'decorators' => $this->defaultFileDecorators
        ));

        // submit button
        $this->addElement('submit', 'submit', array(
            'label' => 'Save',
            'ignore' => true,
            'class' => $this->defaultClass
        ));

    }

    public function populateFromEntity(Products_Model_Product $product)
    {
        $this->populate($product->toArray());
        $picture = $product->getPicture();

        if (!empty($picture)) {
            $this->getElement('picture')->addDecorator('ViewScript', array(
                'viewScript' => 'admin/picture-preview.phtml',
                'product' => $product
            ));
        }

        $categories = [];
        foreach ($product->getCategories() as $category) {
            $categories[] = $category->getId();
        }

        $this->populate([
            'categoryIds' => $categories
        ]);
        return $this;
    }

} 