<?php
/**
 * Created by PhpStorm.
 * User: gopa
 * Date: 10/21/14
 * Time: 3:42 PM
 */

class Products_Form_Property extends Application_Form_AbstractBootstrapable
{

    protected $defaultElementDecorators = [
        'ViewHelper',
        [['element-width' => 'HtmlTag'], ['class' => 'col-xs-6']],
        ['Label', ['class' => 'col-xs-4 control-label', 'placement' => 'prepend']], 'Description', 'Errors',
        [['form-group' => 'HtmlTag'], ['class' => 'form-group']],
        [['row' => 'HtmlTag'], ['class' => 'row']]
    ];

    private function getTypesAsMultiOptions()
    {
        $multiOptions = [];
        $types = Products_Model_Property::getAvailableTypes();
        foreach ($types as $type) {
            $multiOptions[$type] = ucfirst($type);
        }
        return $multiOptions;
    }

    public function init()
    {
        $this->setAttrib('class', 'form-horizontal');

        $this->setMethod('POST');

        $this->setDescription('Set property name');

        // Category name
        $this->addElement('text', 'name', array(
            'label' => 'Property Name:',
            'filters' => array('StringTrim'),
            'class' => 'form-control',
            'required' => true
        ));

        $this->addElement('select', 'type', array(
            'label' => 'Select Property Type:',
            'filters' => array('StringTrim'),
            'class' => 'form-control',
            'multiOptions' => $this->getTypesAsMultiOptions(),
            'required' => true
        ));
    }
} 