<?php
/**
 * Created by PhpStorm.
 * User: gopa
 * Date: 10/7/14
 * Time: 5:19 PM
 */

class Products_Form_SearchProduct extends Application_Form_AbstractBootstrapable
{
    private $inputDecorators = array('ViewHelper');

    public function init()
    {
        $this->setMethod('Get');
        $this->setDescription('Search:');

        $this->addElement('text', 'searchName', array(
            'label' => 'Search',
            'placeholder' => 'Product Name',
            'filters' => array('StringTrim'),
            'decorators' => array(1 => 'Label') + $this->inputDecorators

        ));

        $this->addElement('text', 'searchPriceFrom', array(
            'placeholder' => 'Price From',
            'filters' => array('StringTrim'),
            'decorators' => $this->inputDecorators,
            'size' => 8
        ));

        $this->addElement('text', 'searchPriceTo', array(
            'placeholder' => 'Price To',
            'filters' => array('StringTrim'),
            'decorators' => $this->inputDecorators,
            'size' => 8
        ));

        $this->addElement('text', 'searchQuantFrom', array(
            'placeholder' => 'Quantity From',
            'filters' => array('StringTrim'),
            'decorators' => $this->inputDecorators,
            'size' => 12,
        ));

        $this->addElement('text', 'searchQuantTo', array(
            'placeholder' => 'Quantity To',
            'filters' => array('StringTrim'),
            'decorators' => $this->inputDecorators,
            'size' => 12,
        ));

        $this->addElement('submit', 'submit', array(
            'label' => 'Search',
            'ignore' => true,
            'class' => 'btn btn-primary btn-sm',
            'decorators' => $this->inputDecorators
        ));
    }
} 