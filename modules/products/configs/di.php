<?php
return [

    'Products_Model_CategoriesMapper' => function () {
            $gateway = new Zend_Db_Table('categories');
            return new Products_Model_CategoriesMapper($gateway);
        },
    'Products_Model_ProductsMapper' => function () {
            $gateway = new Zend_Db_Table('products');
            return new Products_Model_ProductsMapper($gateway);
        },
    'Products_Form_Product' => function ($sm) {
            $productForm = new Products_Form_Product($sm->get('Products_Model_CategoriesMapper'));
            return $productForm;
        },
    'Products_Model_ProductsRepository' => function ($sm) {
            $productRepository = new Products_Model_ProductsRepository(
                $sm->get('Products_Model_ProductsMapper'),
                new Products_Model_ProductsCategoriesGateway(), $sm->get('Products_Model_PicturesMapper'));
            return $productRepository;
        },
    'Products_Model_ProductCreation' => function ($sm) {
            $productCreation = new Products_Model_ProductCreation(
                $sm->get('Products_Model_ProductsRepository'),
                new Products_Model_ProductFactory());
            return $productCreation;
        },
    'Products_Model_ProductModification' => function ($sm) {
            $productModification = new Products_Model_ProductModification(
                $sm->get('Products_Model_ProductsRepository'),
                new Products_Model_ProductFactory());
            return $productModification;
        },
    'Products_Model_DeleteCoverPhoto' => function ($sm) {
            $deleteProductPhoto = new Products_Model_DeleteCoverPhoto(
                $sm->get('Products_Model_ProductsMapper'));
            return $deleteProductPhoto;
        },
    'Products_Model_ProductDeleter' => function ($sm) {
            $productDeleter = new Products_Model_ProductDeleter(
                $sm->get('Products_Model_ProductsMapper'),
                $sm->get('Products_Model_DeleteCoverPhoto'));
            return $productDeleter;
        },
    'Products_Model_PicturesMapper' => function ($sm) {
            $gateway = new Zend_Db_Table('product_pictures');
            return new Products_Model_PicturesMapper($gateway);
        },
    'Products_Model_PictureAdder' => function ($sm) {
            $pictureAdder = new Products_Model_PictureAdder($sm->get('Products_Model_PicturesMapper'),
                new Products_Model_PictureFactory());
            return $pictureAdder;
        },

    'Products_Model_PicturesDeleter' => function($sm) {
            $picturesDeleter = new Products_Model_PicturesDeleter(
                $sm->get('Products_Model_PicturesMapper'));
            return $picturesDeleter;
        },
    'Products_Model_PropertiesRepository' => function () {
            $gateway = new Zend_Db_Table('properties');
            return new Products_Model_PropertiesRepository($gateway);
        },


];