<?php
/**
 * Created by PhpStorm.
 * User: gopa
 * Date: 6/10/14
 * Time: 3:14 PM
 */

class Products_Model_Picture {

    /**
     * @var integer
     */
    private $id;

    /**
     * @var integer
     */
    private $productId;

    /**
     * @var string
     */
    private $picture;


    /**
     * Setter for $id
     *
     * @param int $id
     * @return Products_Model_Picture Provides fluent interface
     */
    public function setId($id)
    {
        $this->id = $id;
        return $this;
    }

    /**
     * Getter for $id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Setter for $picture
     *
     * @param string $picture
     * @return Products_Model_Picture Provides fluent interface
     */
    public function setPicture($picture)
    {
        $this->picture = $picture;
        return $this;
    }

    /**
     * Getter for $picture
     *
     * @return string
     */
    public function getPicture()
    {
        return $this->picture;
    }

    /**
     * Setter for $productId
     *
     * @param int $productId
     * @return Products_Model_Picture Provides fluent interface
     */
    public function setProductId($productId)
    {
        $this->productId = $productId;
        return $this;
    }

    /**
     * Getter for $productId
     *
     * @return int
     */
    public function getProductId()
    {
        return $this->productId;
    }

    /**
     * Takes Zend data array with data from DB query
     * @param array $options
     */
    public function __construct(array $options = null)
    {
        if (null != $options) {
            if (isset($options['data'])) {
                $this->populate($options['data']);
            } else {
                $this->populate($options);
            }
        }
    }

    /**
     * Populates entity values (set values from data array in the Entity)
     *
     * @param array $options
     * @return $this
     */

    public function populate(array $options)
    {
        $dashToCamelFilter = new Zend_Filter_Word_DashToCamelCase();
        foreach ($options as $option => $value) {
            $setter = 'set' . ucfirst($dashToCamelFilter->filter($option));
            if (method_exists($this, $setter)) {
                $this->{$setter}($value);
            }
        }
        return $this;
    }


    /**
     * @return array
     */
    public function toArray()
    {
        return get_object_vars($this);
    }




} 