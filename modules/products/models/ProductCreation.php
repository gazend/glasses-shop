<?php
/**
 * Created by PhpStorm.
 * User: gopa
 * Date: 5/2/14
 * Time: 5:16 PM
 */

class Products_Model_ProductCreation
{
    private $productsRepository;
    private $productFactory;

    public function __construct(Products_Model_ProductsRepository $productRepository,
                                Products_Model_ProductFactory $productFactory)
    {
        $this->productsRepository = $productRepository;
        $this->productFactory = $productFactory;
    }

    public function insert(Products_Form_Product $form)
    {
        $product = $this->productFactory->createFromForm($form);
        $this->productsRepository->insert($product);
        return $product;
    }
} 