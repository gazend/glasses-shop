<?php
/**
 * Created by PhpStorm.
 * User: gopa
 * Date: 6/11/14
 * Time: 5:19 PM
 */

class Products_Model_PictureFactory
{

    public function createFromForm(Products_Form_Pictures $picturesForm, $productId)
    {
        $pictures = [];
        foreach($picturesForm->getValue('pictures') as $picture) {
                $pictures[] = new Products_Model_Picture([
                    'picture' => $picture,
                    'productId' => $productId
                ]);
        }

        return $pictures;
    }
} 