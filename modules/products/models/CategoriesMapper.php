<?php
/**
 * Mapper for "categories" table
 *
 * @author Georgi Ivanov <gopa_bg@abv.bg>
 * @category Products
 * @package Products Models
 * @copyright GAZE-ND
 */

class Products_Model_CategoriesMapper
{

    /**
     * Table data gateway
     * @var Zend_Db_Table_Abstract
     */
    private $gateway;

    /**
     * Constructor
     *
     * Sets dependencies
     *
     * @param Zend_Db_Table_Abstract $gateway
     */
    public function __construct(Zend_Db_Table_Abstract $gateway)
    {
        $gateway->setRowClass('Products_Model_Category');
        $this->gateway = $gateway;
    }

    /**
     * Insert new row in Category table
     *
     * @param Products_Model_Category $entity
     * @return $this
     */
    public function insert(Products_Model_Category $entity)
    {
        $entity->setId(null);
        $data = $entity->toArray();
        $id = $this->gateway->insert($data);
        $entity->setId($id);
        return $this;
    }

    /**
     * Updates category name
     *
     * @param Products_Model_Category $entity
     * @return $this
     */
    public function update(Products_Model_Category $entity)
    {
        $id = $entity->getId();
        if (empty($id)) {
            return $this;
        }
        $adapter = $this->gateway->getAdapter();
        $data = $entity->toArray();
        $where = $adapter->quoteInto('id = ?', $id, 'INTEGER');
        $this->gateway->update($data, $where);
        return $this;
    }

    /**
     * Delete row from Categoy table
     *
     * @param Integer $id
     * @return $this
     */
    public function delete($id)
    {

        if(empty($id)){
            return $this;
        }
        $adapter = $this->gateway->getAdapter();
        $where = $adapter->quoteInto('id = ?', $id);
        $this->gateway->delete($where);
        return $this;
    }

    /**
     * Get all categories
     *
     * @return Zend_Db_Table_Rowset_Abstract|Products_Model_Category[]
     */
    public function getAllCategories()
    {
        return $this->gateway->fetchAll(null, 'id DESC');
    }

    /**
     * Get Select object for Categories table
     *
     * @return Zend_Db_Table_Select
     */
    public function getSelectForAllCategories()
    {
        $select = $this->gateway->select();
        $select->from($this->gateway, ['id', 'name']);
        return $select;
    }

} 