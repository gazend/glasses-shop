<?php
/**
 * Created by PhpStorm.
 * User: gopa
 * Date: 4/22/14
 * Time: 4:09 PM
 */

class Products_Model_Product
{
    /**
     *Product Id
     *
     * @var integer
     */
    private $id;

    /**
     * Product Name
     *
     * @var string
     */
    private $name;

    /**
     * Product Price
     *
     * @var float
     */
    private $price;

    /**
     * Product description
     *
     * @var string
     */
    private $description;

    /**
     * Product amount
     *
     * @var integer
     */
    private $amountAvailable;

    /**
     * Product Picture
     *
     * @var string
     */
    private $picture;

    /**
     * User id
     *
     * @var integer
     */
    private $userId;

    /**
     * @var Products_Model_Picture[]
     */
    private $pictures = [];

    /**
     * @var Products_Model_Category[]
     */
    private $categories = [];

    public function __construct(array $options = null)
    {
        if(null != $options){
            if(isset($options['data'])){
            $this->populate($options['data']);
            } else {
                $this->populate($options);
            }
        }
    }

    /**
     * Populates entity values
     *
     * @param array $options
     * @return $this
     */
    public function populate(array $options)
    {
        $dashToCamelFilter = new Zend_Filter_Word_DashToCamelCase();
        foreach ($options as $option => $value) {
            $setter = 'set' . ucfirst($dashToCamelFilter->filter($option));
            if (method_exists($this, $setter)) {
                $this->{$setter}($value);
            }
        }
        return $this;
    }

    /**
     * Setter for $amountAvailable
     *
     * @param int $amountAvailable
     * @return Products_Model_Product Provides fluent interface
     */
    public function setAmountAvailable($amountAvailable)
    {
        $this->amountAvailable = $amountAvailable;
        return $this;
    }

    /**
     * Getter for $amountAvailable
     *
     * @return int
     */
    public function getAmountAvailable()
    {
        return $this->amountAvailable;
    }

    /**
     * Setter for $description
     *
     * @param string $description
     * @return Products_Model_Product Provides fluent interface
     */
    public function setDescription($description)
    {
        $this->description = $description;
        return $this;
    }

    /**
     * Getter for $description
     *
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Setter for $id
     *
     * @param int $id
     * @return Products_Model_Product Provides fluent interface
     */
    public function setId($id)
    {
        $this->id = $id;
        return $this;
    }

    /**
     * Getter for $id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Setter for $name
     *
     * @param string $name
     * @return Products_Model_Product Provides fluent interface
     */
    public function setName($name)
    {
        $this->name = $name;
        return $this;
    }

    /**
     * Getter for $name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Setter for $picture
     *
     * @param string $picture
     * @return Products_Model_Product Provides fluent interface
     */
    public function setPicture($picture)
    {
        $this->picture = $picture;
        return $this;
    }

    /**
     * Getter for $picture
     *
     * @return string
     */
    public function getPicture()
    {
        return $this->picture;
    }

    /**
     * Setter for $price
     *
     * @param float $price
     * @return Products_Model_Product Provides fluent interface
     */
    public function setPrice($price)
    {
        $this->price = $price;
        return $this;
    }

    /**
     * Getter for $price
     *
     * @return float
     */
    public function getPrice()
    {
        return $this->price;
    }

    /**
     * Setter for $userId
     *
     * @param int $userId
     * @return Products_Model_Product Provides fluent interface
     */
    public function setUserId($userId)
    {
        $this->userId = $userId;
        return $this;
    }

    /**
     * Getter for $userId
     *
     * @return int
     */
    public function getUserId()
    {
        return $this->userId;
    }

    /**
     * Converts Entity to array
     *
     * @return array
     */
    public function toArray()
    {
        return get_object_vars($this);
    }

    /**
     * Add category to product
     *
     * @param Products_Model_Category $category
     * @return $this
     */
    public function addCategory(Products_Model_Category $category)
    {
        $this->categories[] = $category;
        return $this;
    }

    /**
     * Add categories to product
     *
     * @param Products_Model_Category[] $categories
     * @return $this
     */
    public function addCategories(array $categories)
    {
        foreach ($categories as $category) {
            $this->addCategory($category);
        }
        return $this;
    }

    /**
     * Empty category array
     *
     * @return $this
     */
    public function unsetCategories()
    {
        $this->categories = [];
        return $this;
    }

    /**
     * Delete previous categories and add new ones
     * @param Products_Model_Category[] $categories
     * @return $this
     */
    public function setCategories(array $categories)
    {
        $this->unsetCategories();
        $this->addCategories($categories);
        return $this;
    }

    /**
     * Get all categories
     *
     * @return Products_Model_Category[]
     */
    public function getCategories()
    {
        return $this->categories;
    }

    /**
     * @param Products_Model_Picture $picture
     * @return $this
     */
    public function addPicture(Products_Model_Picture $picture)
    {
        $this->pictures[] = $picture;
        return $this;
    }

    /**
     * @param Products_Model_Picture[] $pictures
     * @return $this
     */
    public function addPictures(array $pictures)
    {
        foreach ($pictures as $picture) {
            $this->addPicture($picture);
        }
        return $this;
    }

    /**
     * @return $this
     */
    public function  clearPictures()
    {
        $this->pictures = [];
        return $this;
    }

    /**
     * @param Products_Model_Picture[] $pictures
     * @return $this
     */
    public function setPictures(array $pictures)
    {
        $this->clearPictures();
        $this->addPictures($pictures);
        return $this;
    }

    /**
     * Getter for $pictures
     *
     * @return \Products_Model_Picture[]
     */
    public function getPictures()
    {
        return $this->pictures;
    }

} 