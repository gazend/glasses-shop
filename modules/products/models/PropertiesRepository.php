<?php
/**
 * Created by PhpStorm.
 * User: gopa
 * Date: 10/15/14
 * Time: 3:53 PM
 */

class Products_Model_PropertiesRepository
{

    private $gateway;

    public function __construct(Zend_Db_Table_Abstract $gateway)
    {
        $gateway->setRowClass('Products_Model_Property');
        $this->gateway = $gateway;
    }

    public function insert (Products_Model_Property $property)
    {
        $property->setId(null);
        $data = $property->toArray();
        $id = $this->gateway->insert($data);
        $property->setId($id);

        return $this;
    }

    public function update(Products_Model_Property $property)
    {
        $id = $property->getId();
        if(empty($id)){
            return $this;
        }
        $adapter = $this->gateway->getAdapter();
        $data = $property->toArray();
        $where = $adapter->quoteInto('id', $id, 'INTEGER');
        $this->gateway->update($data, $where);

        return $this;
    }

    public function delete($id)
    {
        if(empty($id)){
            return $this;
        }
        $adapter = $this->gateway->getAdapter();

        $where = $adapter->quoteInto('id', $id, 'INTEGER');
        $this->gateway->delete($where);

        return $this;
    }

    public function findById($id)
    {
        $select = $this->gateway->select()
                                ->where('id', $id, 'INTEGER');
        return $this->gateway->fetchRow($select);
    }

    public function getAllProperties()
    {
        return $this->gateway->fetchAll();
    }

}