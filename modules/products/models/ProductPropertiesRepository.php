<?php
/**
 * Created by PhpStorm.
 * User: gopa
 * Date: 10/16/14
 * Time: 11:14 AM
 */

class Products_Model_ProductPropertiesRepository
{
    /**
     * @var Products_Model_ProductPropertiesGateway
     */
    private $ppGateway;

    public function __construct(
        Products_Model_ProductPropertiesGateway $ppGateway)
    {
        $this->ppGateway = $ppGateway;
    }

    public function insert($productId, $propertyId, $value)
    {
        $this->ppGateway->insert([
            'productId' => $productId,
            'propertyId' => $propertyId,
            'value' => $value
        ]);

        return $this;
    }

    public function update($productId, $propertyId, $value)
    {
        $this->ppGateway->update([
            'value' => $value
        ], [
            'productId = ?' => $productId,
            'propertyId = ?' => $propertyId,
        ]);

        return $this;
    }

    /**
     * @param array $rawArray
     * @return Products_Model_ProductProperty
     */
    private function createEntityFromRawArray(array $rawArray)
    {
        $productProperty = new Products_Model_ProductProperty($rawArray);
        $property = new Products_Model_Property(array(
            'id' => $rawArray['propertyId'],
            'name' => $rawArray['name'],
            'type' => $rawArray['type']
        ));
        $productProperty->setProperty($property);

        return $productProperty;
    }

    /**
     * @return Zend_Db_Table_Select
     */
    private function builtSelect()
    {
        $select = $this->ppGateway->select();
        $select->setIntegrityCheck(false);

        $select->from(array('pp' => 'product_properties'),
            ['id', 'value', 'productId', 'propertyId'])
            ->joinLeft(array('p' => 'properties'),
                'pp.propertyId = p.id',
                ['name', 'type']);
        return $select;
    }

    /**
     * @param integer $productId
     * @return Products_Model_ProductProperty[]
     */
    public function getByProductId($productId)
    {
        $select = $this->builtSelect();

        $select->where('pp.productId = ?', $productId, 'INTEGER');

        $adapter = $this->ppGateway->getAdapter();
        $result = $adapter->fetchAll($select);

        /** @var Products_Model_ProductProperty[] $properties */
        $properties = array();
        foreach ($result as $rawArray) {
            $properties[] = $this->createEntityFromRawArray($rawArray);
        }
        return $properties;
    }



} 