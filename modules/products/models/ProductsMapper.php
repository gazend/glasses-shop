<?php
/**
 * Created by PhpStorm.
 * User: gopa
 * Date: 4/22/14
 * Time: 3:46 PM
 */

class Products_Model_ProductsMapper
{
    /**
     * Table data gateway
     *
     * @var Zend_Db_Table_Abstract
     */
    private $gateway;

    /**
     * Constructor
     *
     * @param Zend_Db_Table_Abstract $gateway
     */
    public function __construct(Zend_Db_Table_Abstract $gateway)
    {
        $gateway->setRowClass('Products_Model_Product');
        $this->gateway = $gateway;
    }

    /**
     * Insert row in Products table
     *
     * @param Products_Model_Product $entity
     * @return $this
     */
    public function insert(Products_Model_Product $entity)
    {
        $entity->setId(null);
        $data = $entity->toArray();
        unset($data['categories']);
        unset($data['pictures']);
        $id = $this->gateway->insert($data);
        $entity->setId($id);

        return $this;
    }

    /**
     * Update row in Products table
     *
     * @param Products_Model_Product $entity
     * @return $this
     */
    public function update(Products_Model_Product $entity)
    {
        $id = $entity->getId();
        if (empty($id)) {
            return $this;
        }
        $adapter = $this->gateway->getAdapter();
        $data = $entity->toArray();
        unset($data['categories']);
        if (empty($data['picture'])) {
            unset($data['picture']);
        }
        $where = $adapter->quoteInto('id = ?', $id, 'INTEGER');
        $this->gateway->update($data, $where);
        return $this;
    }

    /**
     * Reset picture from product on delete
     * @param $productId
     * @return $this
     */
    public function resetPicture($productId)
    {
        $data = ['picture' => null];
        $adapter = $this->gateway->getAdapter();
        $where = $adapter->quoteInto('id = ?', $productId, 'INTEGER');
        $this->gateway->update($data, $where);
        return $this;
    }

    /**
     * Delete row from Product table
     *
     * @param $id
     * @return $this
     */
    public function delete($id)
    {
        if (empty($id)) {
            return $this;
        }

        $adapter = $this->gateway->getAdapter();
        $where = $adapter->quoteInto('id = ?', $id);
        $this->gateway->delete($where);
        return $this;
    }

    public function getAllProducts()
    {
        return $this->gateway->fetchAll();
    }

    /**
     * @param integer $id
     * @return null|Products_Model_Product
     */
    public function findById($id)
    {
        $select = $this->gateway->select()
            ->where('id = ?', $id, 'INTEGER');
        return $this->gateway->fetchRow($select);
    }

    public function searchProducts($productName, $priceFrom, $priceTo, $qntyFrom, $qntyTo)
    {
        $select = $this->gateway->select();
        if ($productName != null) {
            $select->where('name LIKE ?', "%$productName%");
        }
        if ($priceFrom != null) {
            $select->where('price > ?', $priceFrom);
        }
        if ($priceTo != null) {
            $select->where('price < ?', $priceTo);
        }
        if ($qntyFrom != null) {
            $select->where('amountAvailable > ?', $qntyFrom);
        }
        if ($qntyTo != null) {
            $select->where('amountAvailable < ?', $qntyTo);
        }
        return $this->gateway->fetchAll($select);
    }
} 