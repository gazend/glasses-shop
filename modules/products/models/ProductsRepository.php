<?php
/**
 * Created by PhpStorm.
 * User: gopa
 * Date: 5/2/14
 * Time: 3:41 PM
 */

class Products_Model_ProductsRepository
{
    private $productsMapper;
    private $productsCategoriesGateway;
    private $picturesMapper;

    public function __construct(
        Products_Model_ProductsMapper $productsMapper,
        Products_Model_ProductsCategoriesGateway $productsCategoriesGateway,
        Products_Model_PicturesMapper $picturesMapper)
    {
        $this->productsMapper = $productsMapper;
        $this->productsCategoriesGateway = $productsCategoriesGateway;
        $this->picturesMapper = $picturesMapper;
    }

    public function insert(Products_Model_Product $product)
    {
        $this->productsMapper->insert($product);

        foreach ($product->getCategories() as $category) {
            $this->productsCategoriesGateway->insert([
                'productId' => $product->getId(),
                'categoryId' => $category->getId()
            ]);
        }
        return $this;
    }

    public function findById($id)
    {
        $product = $this->productsMapper->findById($id);
        $categories = $this->productsCategoriesGateway->fetchAll([
            'productId = ?' => $id
        ]);
        foreach ($categories as $category) {
            $product->addCategory(new Products_Model_Category([
                'id' => $category->categoryId
            ]));
        }
        $product->setPictures($this->picturesMapper->getByProductId($id));

        return $product;
    }

    public function update(Products_Model_Product $product)
    {
        $this->productsMapper->update($product);

        foreach ($product->getCategories() as $category) {
            $this->productsCategoriesGateway->insert([
                'productId' => $product->getId(),
                'categoryId' => $category->getId()
            ]);
        }
        return $this;
    }

} 