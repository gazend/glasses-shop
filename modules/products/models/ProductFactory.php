<?php
/**
 * Created by PhpStorm.
 * User: gopa
 * Date: 5/2/14
 * Time: 4:47 PM
 */

class Products_Model_ProductFactory
{
    public function createFromForm(Products_Form_Product $productForm)
    {
        $product = new Products_Model_Product($productForm->getValues());
        $product->setUserId(Zend_Auth::getInstance()->getIdentity()->id);


        foreach ($productForm->getValue('categoryIds') as $categoryId) {
            $product->addCategory(new Products_Model_Category([
                'id' => $categoryId
            ]));
        }
        return $product;
    }

} 