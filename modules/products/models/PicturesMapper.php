<?php
/**
 * Created by PhpStorm.
 * User: gopa
 * Date: 6/10/14
 * Time: 4:20 PM
 */

class Products_Model_PicturesMapper {

    /**
     * @var Zend_Db_Table_Abstract
     */
    private $gateway;

    /**
     * @param Zend_Db_Table_Abstract $gateway
     */
    public function __construct(Zend_Db_Table_Abstract $gateway)
    {
       // Attach our Picture entity with the DB!!! not using standard ZF entities
       $gateway->setRowClass('Products_Model_Picture');
        $this->gateway = $gateway;
    }

    /**
     * @param Products_Model_Picture $picture
     * @return $this
     */
    public function insert(Products_Model_Picture $picture)
    {
        $picture->setId(null);
        $data = $picture->toArray();
        $id = $this->gateway->insert($data);
        $picture->setId($id);
        return $this;

    }

    /**
     * @param Products_Model_Picture $picture
     * @return $this
     */
    public function update(Products_Model_Picture $picture)
    {
        $id = $picture->getId();
        if( empty($id)) {
            return $this;
        }
        $adapter = $this->gateway->getAdapter();
        $where = $adapter->quoteInto('id =?', $id);
        $data = $picture->toArray();
        $this->gateway->update($data, $where);

        return $this;
    }

    /**
     * @param $id
     * @return $this
     */
    public function delete($id)
    {
        if(empty($id)){
            return $this;
        }
        $adapter = $this->gateway->getAdapter();

       $where = $adapter->quoteInto('id = ?', $id);
        $this->gateway->delete($where, $id);
        return $this;
    }

     /**
     * @param $id
     * @return null|Products_Model_Picture
     */
    public function findById($id)
    {
        $select = $this->gateway->select()
                                ->where('id = ?', $id, 'INTEGER');
        return $this->gateway->fetchRow($select);
    }

    /**
     * @param Zend_Db_Table_Rowset_Abstract $rowSet
     * @return Products_Model_Picture[]
     */
    private function extractEntities(Zend_Db_Table_Rowset_Abstract $rowSet)
    {
        $entities = [];
        foreach ($rowSet as $row) {
            $entities[] = $row;
        }
        return $entities;
    }

    /**
     * @param $productId
     * @return Products_Model_Picture[]
     */
    public function getByProductId($productId)
    {
        $select = $this->gateway->select()
            ->where('productId = ?', $productId, 'INTEGER');
        $rowSet = $this->gateway->fetchAll($select);
        return $this->extractEntities($rowSet);
    }

    /***
     * @param int[] $pictureIds
     * @return Products_Model_Picture[]
     */
    public function getByIds(array $pictureIds)
    {
        $select = $this->gateway->select()
            ->where('id IN (?)', $pictureIds, 'INTEGER');
        $pictures = $this->extractEntities($this->gateway->fetchAll($select));

        return $pictures;

    }

    public function deleteByIds(array $pictureIds)
    {
        $adapter = $this->gateway->getAdapter();
        $where = $adapter->quoteInto('id IN (?)', $pictureIds, 'INTEGER');
        $this->gateway->delete($where);

        return $this;
    }

} 