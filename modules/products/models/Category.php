<?php
/**
 * Created by PhpStorm.
 * User: gopa
 * Date: 4/4/14
 * Time: 11:49 AM
 */

class Products_Model_Category {

    /**
     * Category Id
     *
     * @var integer
     */
    private $id;

    /**
     * Category Name
     *
     * @var string
     */
    private $name;

    /**
     * Constructor
     *
     * Sets entity's property values
     *
     * @param array $options
     */
    public function __construct(array $options = null)
    {
        if (null != $options) {
            if (isset($options['data'])) {
                $this->populate($options['data']);
            } else {
                $this->populate($options);
            }
        }
    }

    /**
     * Populates entity values
     *
     * @param array $options
     * @return $this
     */
    public function populate(array $options)
    {
        $dashToCamelFilter = new Zend_Filter_Word_DashToCamelCase();
        foreach ($options as $option => $value) {
            $setter = 'set' . ucfirst($dashToCamelFilter->filter($option));
            if (method_exists($this, $setter)) {
                $this->{$setter}($value);
            }
        }
        return $this;
    }

    /**
     * Setter for $id
     *
     * @param int $id
     * @return Products_Model_Category Provides fluent interface
     */
    public function setId($id)
    {
        $this->id = $id;
        return $this;
    }

    /**
     * Getter for $id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Setter for $name
     *
     * @param string $name
     * @return Products_Model_Category Provides fluent interface
     */
    public function setName($name)
    {
        $this->name = $name;
        return $this;
    }

    /**
     * Getter for $name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Converts entity into array
     *
     * @return array
     */
    public function toArray()
    {
        return get_object_vars($this);
    }
} 