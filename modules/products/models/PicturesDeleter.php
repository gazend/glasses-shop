<?php
/**
 * Created by PhpStorm.
 * User: gopa
 * Date: 7/14/14
 * Time: 4:21 PM
 */

class Products_Model_PicturesDeleter
{
    private $picturesMapper;

    public function __construct(Products_Model_PicturesMapper $picturesMapper)
    {
        $this->picturesMapper = $picturesMapper;
    }

    // TODO refactor to get physical files removed in another private method

    /**
     * @param Products_Model_Picture[] $pictures
     */
    private function deletePhysicalFiles(array $pictures)
    {
        foreach($pictures as $picture) {
            $pictureName = $picture->getPicture();
            if(!empty($pictureName)) {

                $prefixPath = ROOT_PATH . '/public/uploads/products/';
                $picturesPaths = [
                    $prefixPath . $pictureName,
                    $prefixPath . '/thumbs/'. $pictureName
                ];

                foreach ($picturesPaths as $picturesPath) {
                    if(file_exists($picturesPath)) {
                        unlink($picturesPath);
                    }
                }
            }
        }
    }

    public function delete($pictureIds)
    {
        $pictures = $this->picturesMapper->getByIds($pictureIds);
        $this->deletePhysicalFiles($pictures);
        $this->picturesMapper->deleteByIds($pictureIds);

        return $this;
    }
} 