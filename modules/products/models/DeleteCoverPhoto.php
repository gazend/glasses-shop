<?php
/**
 * Created by PhpStorm.
 * User: gopa
 * Date: 6/9/14
 * Time: 5:31 PM
 */

class Products_Model_DeleteCoverPhoto {

    private $productMapper;

    public function __construct(Products_Model_ProductsMapper $productMapper)
    {
        $this->productMapper = $productMapper;
    }

    public function delete($id)
    {
        $product = $this->productMapper->findById($id);
        if(empty($product)) {
            throw new Products_Model_ProductNotFoundException('Product not found!');
        }
        $this->cleanFiles($product);
        $this->productMapper->resetPicture($id);
    }

    public function cleanFiles(Products_Model_Product $product)
    {

        $picture = $product->getPicture();
        if(!empty($picture)) {
        $prefixPath = ROOT_PATH . '/public/uploads/products/';
        $pictures = [$prefixPath . $picture, $prefixPath . '/thumbs/'. $picture];

        foreach ($pictures as $picture) {
            if(file_exists($picture)) {
                unlink($picture);
            }
        }
        }
    }


} 