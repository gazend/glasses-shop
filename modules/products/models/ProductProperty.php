<?php
/**
 * Created by PhpStorm.
 * User: gopa
 * Date: 10/14/14
 * Time: 4:54 PM
 */

class Products_Model_ProductProperty
{
    /**
     * @var integer
     */
    private $id;

    /**
     * @var integer
     */
    private $productId;

    /**
     * @var Products_Model_Property
     */
    private $property;

    /**
     * @var string
     */
    private $value;

    /**
     * Setter for $id
     *
     * @param int $id
     * @return Products_Model_ProductProperty Provides fluent interface
     */
    public function setId($id)
    {
        $this->id = $id;
        return $this;
    }

    /**
     * Getter for $id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Setter for $productId
     *
     * @param int $productId
     * @return Products_Model_ProductProperty Provides fluent interface
     */
    public function setProductId($productId)
    {
        $this->productId = $productId;
        return $this;
    }

    /**
     * Getter for $productId
     *
     * @return int
     */
    public function getProductId()
    {
        return $this->productId;
    }

    /**
     * Setter for $property
     *
     * @param \Products_Model_Property $property
     * @return Products_Model_ProductProperty Provides fluent interface
     */
    public function setProperty(Products_Model_Property $property)
    {
        $this->property = $property;
        return $this;
    }

    /**
     * Getter for $property
     *
     * @return \Products_Model_Property
     */
    public function getProperty()
    {
        return $this->property;
    }

    /**
     * Setter for $value
     *
     * @param string $value
     * @return Products_Model_ProductProperty Provides fluent interface
     */
    public function setValue($value)
    {
        $this->value = $value;
        return $this;
    }

    /**
     * Getter for $value
     *
     * @return string
     */
    public function getValue()
    {
        return $this->value;
    }

    /**
     * @param array $options
     */
    public function __construct(array $options = null)
    {
        if (null != $options) {
            if (isset($options['data'])) {
                $this->populate($options['data']);
            } else {
                $this->populate($options);
            }
        }
    }

    /**
     * Populates entity values
     *
     * @param array $options
     * @return $this
     */
    public function populate(array $options)
    {
        $dashToCamelFilter = new Zend_Filter_Word_DashToCamelCase();
        foreach ($options as $option => $value) {
            $setter = 'set' . ucfirst($dashToCamelFilter->filter($option));
            if (method_exists($this, $setter)) {
                $this->{$setter}($value);
            }
        }
        return $this;
    }

} 