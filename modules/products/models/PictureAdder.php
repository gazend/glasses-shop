<?php
/**
 * Created by PhpStorm.
 * User: gopa
 * Date: 6/23/14
 * Time: 5:09 PM
 */

class Products_Model_PictureAdder {

    private $picturesMapper;
    private $pictureFactory;

    public function __construct(Products_Model_PicturesMapper $picturesMapper,
                                Products_Model_PictureFactory $pictureFactory)
    {
        $this->picturesMapper = $picturesMapper;
        $this->pictureFactory = $pictureFactory;
    }

    public function insert(Products_Form_Pictures $pictures, $productId)
    {
        $picturesMapper = $this->picturesMapper;
        $pictureFactory = $this->pictureFactory;

        $pictures = $pictureFactory->createFromForm($pictures, $productId);

        foreach($pictures as $picture) {
            $picturesMapper->insert($picture);
        }
        return $this;
    }

} 