<?php
/**
 * Created by PhpStorm.
 * User: gopa
 * Date: 10/14/14
 * Time: 4:22 PM
 */

class Products_Model_Property
{
    /**
     * @var integer
     */
    private $id;

    /**
     * @var string
     */
    private $name;

    /**
     * @var string
     */
    private $type;

    /**
     * @var string[]
     */
    private static $propertyTypes = ['color','measures', 'weight', 'other'];

    /**
     * @param array $options
     */
    public function __construct(array $options = null)
    {
        if (null != $options) {
            if (isset($options['data'])) {
                $this->populate($options['data']);
            } else {
                $this->populate($options);
            }
        }
    }

    /**
     * Populates entity values
     *
     * @param array $options
     * @return $this
     */
    public function populate(array $options)
    {
        $dashToCamelFilter = new Zend_Filter_Word_DashToCamelCase();
        foreach ($options as $option => $value) {
            $setter = 'set' . ucfirst($dashToCamelFilter->filter($option));
            if (method_exists($this, $setter)) {
                $this->{$setter}($value);
            }
        }
        return $this;
    }

    /**
     * Setter for $id
     *
     * @param int $id
     * @return Products_Model_Property Provides fluent interface
     */
    public function setId($id)
    {
        $this->id = $id;
        return $this;
    }

    /**
     * Getter for $id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Setter for $name
     *
     * @param string $name
     * @return Products_Model_Property Provides fluent interface
     */
    public function setName($name)
    {
        $this->name = $name;
        return $this;
    }

    /**
     * Getter for $name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Setter for $type
     *
     * @param string $type
     * @throws InvalidArgumentException When type is wrong
     * @return Products_Model_Property Provides fluent interface
     */
    public function setType($type)
    {
        if (!in_array($type, self::$propertyTypes)){
            throw new InvalidArgumentException(
                "Type '{$type}' is not allowed as property type");
        }

        $this->type = $type;
        return $this;
    }

    /**
     * Getter for $type
     *
     * @return string
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * Converts Entity to array
     *
     * @return array
     */
    public function toArray()
    {
        return get_object_vars($this);
    }

    public static function getAvailableTypes()
    {
        return self::$propertyTypes;
    }

} 