<?php
/**
 * Created by PhpStorm.
 * User: gopa
 * Date: 6/10/14
 * Time: 11:23 AM
 */

class Products_Model_ProductDeleter
{
    private $productMapper;
    private $productPhotoDeleter;

    public function __construct(Products_Model_ProductsMapper $productMapper,
                                Products_Model_DeleteCoverPhoto $productPhotoDeleter)
    {
        $this->productMapper = $productMapper;
        $this->productPhotoDeleter = $productPhotoDeleter;
    }

    public function delete($id)
    {
        $product = $this->productMapper->findById($id);
        if (empty($product)) {
            throw new Products_Model_ProductNotFoundException('Product not found!');
        }
        $this->productPhotoDeleter->cleanFiles($product);
        $this->productMapper->delete($id);
    }

} 