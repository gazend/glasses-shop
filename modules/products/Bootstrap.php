<?php
/**
 * Created by PhpStorm.
 * User: gopa
 * Date: 11/22/13
 * Time: 6:59 PM
 */

class Products_Bootstrap extends Zend_Application_Module_Bootstrap
{
    protected function _initModuleAutoloader()
    {
        $autoloader = new Zend_Application_Module_Autoloader(array(
            'namespace' => 'Products_',
            'basePath' => __DIR__
        ));
        return $autoloader;
    }
} 