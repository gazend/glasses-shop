<?php
/**
 * Glasses Shop ©
 * Copyright © 2012 Georgi Ivanov <gopa_bg@abv.bg>
 *
 * LICENSE
 *
 * A copy of this license is bundled with this package in the file LICENSE.txt.
 *
 * Copyright © Glasses Shop
 *
 * Platform that uses this site is protected by copyright.
 * It is provided solely for the use of this site and all its copying,
 * processing or use of parts thereof is prohibited and pursued by law.
 *
 * @author Georgi Ivanov <gopa_bg@abv.bg>
 * @category Admin
 * @package Admin Controllers
 * @copyright GAZE-ND
 */

/**
 * Admin Controller
 *
 * @author Georgi Ivanov <gopa_bg@abv.bg>
 * @category Profile
 * @package Profile Controllers
 * @copyright GAZE-ND
 */

class Profile_AdminController extends GZend_Controller_AdminActions
{
    
    public function indexAction()
    {
        /* @var $form Profile_Form_EditProfile */
        $form = $this->serviceManager->get('ProfileCreateEditProfile');
        $this->view->form = $form;

        $request = $this->getRequest();
        if ($request->isPost() && $form->isValid($request->getPost())) {
            $user = new Application_Model_AdminUser($form->getValues());
            $user->setId(Zend_Auth::getInstance()->getIdentity()->id);
            /* @var $usersMapper Application_Model_UsersMapper */
            $usersMapper = $this->serviceManager->get('Application_Model_UsersMapper');
            $usersMapper->updateProfile($user);
            $this->view->success = true;
        }
    }

    public function changePasswordAction()
    {
        $form = new Profile_Form_ChangePassword();
        $this->view->form = $form;

        $request = $this->getRequest();
        if ($request->isPost() && $form->isValid($request->getPost())) { //fill data in the form

            $userId = Zend_Auth::getInstance()->getIdentity()->id;

            $passOldHash = $form->getValue('oldPass');
            $formPassNew = $form->getValue('newPass');
            /** @var Profile_Model_ChangePassword $passChange */
            $passChange = $this->serviceManager->get('Profile_Model_ChangePassword');

            if ($passChange->changePassword($userId, $passOldHash, $formPassNew)) {

                $this->view->success = true;
            } else {
                $this->view->error = true;
            }

        }

    }
}
