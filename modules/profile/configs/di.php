<?php
return [

    'Profile_Form_EditProfileFactory' => function ($sm) {
        return new Profile_Form_EditProfileFactory(
            $sm->get('Application_Model_UsersMapper')
        );
    },

    'ProfileCreateEditProfile' => function ($sm) {
        return $sm->get('Profile_Form_EditProfileFactory')->create();
    },

    'Profile_Model_ChangePassword' => function ($sm) {

        return new Profile_Model_ChangePassword(
            $sm->get('Application_Model_UsersMapper')
        );
    }
];