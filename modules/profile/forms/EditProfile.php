<?php


class Profile_Form_EditProfile extends Zend_Form
{
    public function init()
    {
        // set the form method to POST
        $this->setMethod('Post');

        $this->setDescription('Edit your profile Info:');

        //Username field
        $this->addElement('text', 'username', array(
            'label' => 'Username:',
            'filters' => array('StringTrim'),
            'required' => true,
        ));

        // Name field
        $this->addElement('text', 'firstName', array(
            'label' => 'Name:',
            'required' => true,
            'filters' => array('StringTrim')
        ));

        //Fname field
        $this->addElement('text', 'lastName', array(
            'label' => 'Family Name:',
            'filters' => array('StringTrim'),
            'required' => true
        ));

        //email field
        $this->addElement('text', 'email', array(
            'label' => 'Email:',
            'required' => true,
            'validators' => array('EmailAddress')
        ));

        $this->addElement('submit', 'submit', array(
            'label' => 'Edit',
            'ignore' => true
        ));


    }

} 