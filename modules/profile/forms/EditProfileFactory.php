<?php

class Profile_Form_EditProfileFactory
{
    /**
     * Users data mapper
     *
     * @var Application_Model_UsersMapper
     */
    private $usersMapper;

    /**
     * Constructor
     *
     * Sets Data Mapper
     *
     * @param Application_Model_UsersMapper $usersMapper
     */
    public function __construct(Application_Model_UsersMapper $usersMapper)
    {
        $this->usersMapper = $usersMapper;
    }

    /**
     * Creates Edit Profile Form
     *
     * @return Profile_Form_EditProfile
     */
    public function create()
    {
        $userId = Zend_Auth::getInstance()->getIdentity()->id;
        $usersMapper = $this->usersMapper;
        $user = $usersMapper->findById($userId);

        $form = new Profile_Form_EditProfile();
        $form->populate($user->toArray());
        return $form;
    }
} 