<?php
/**
 * Created by PhpStorm.
 * User: gopa
 * Date: 11/28/13
 * Time: 3:13 PM
 */

class Profile_Form_ChangePassword extends Zend_Form
{
    public function init()
    {
        $this->setMethod('POST');
        $this->setDescription('Change Your Password');

        // old password
        $this->addElement('password', 'oldPass', array(
            'label' => 'Old Password:',
            'required' => true,
        ));

        //new password
        $this->addElement('password', 'newPass', array(
            'label' => 'New password',
            'required' => true,
        ));

        $sameAs = new GZend_Validate_SameAs(['element' => $this->getElement('newPass')]);
        //compare new pass
        $this->addElement('password', 'newPassRepeat', array(
            'label' => 'Repeat password',
            'required' => true,
            'validators' => array($sameAs),
        ));

        // submit button
        $this->addElement('submit', 'submit', array(
            'label' => 'submit',
            'ignore' => true
        ));
    }
} 