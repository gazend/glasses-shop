<?php


class Profile_Model_ChangePassword
{

    private $usersMapper;

    public function __construct(Application_Model_UsersMapper $usersMapper)
    {
        $this->usersMapper = $usersMapper;
    }

    public function changePassword($userId, $userInputPass, $newPass)
    {
        $user = $this->usersMapper->findById($userId);
        if (sha1($userInputPass) == $user->getPassword()) {
            $user->setPassword($newPass);
            $this->usersMapper->updatePass($user);
            return true;
        }
        return false;
    }


} 