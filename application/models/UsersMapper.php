<?php
/**
 * Glasses Shop ©
 * Copyright © 2012 Georgi Ivanov <gopa_bg@abv.bg>
 *
 * LICENSE
 *
 * A copy of this license is bundled with this package in the file LICENSE.txt.
 *
 * Copyright © Glasses Shop
 *
 * Platform that uses this site is protected by copyright.
 * It is provided solely for the use of this site and all its copying,
 * processing or use of parts thereof is prohibited and pursued by law.
 *
 * @author Georgi Ivanov <gopa_bg@abv.bg>
 * @category Application
 * @package Application Models
 * @copyright GAZE-ND
 */

/**
 * Mapper for "users" table 
 *
 * @author Georgi Ivanov <gopa_bg@abv.bg>
 * @category Application
 * @package Application Models
 * @copyright GAZE-ND
 */ 
class Application_Model_UsersMapper
{
    
    /**
     * Table data gateway
     * 
     * @var Zend_Db_Table_Abstract 
     */
    private $gateway;

    /**
     * Constructor
     * 
     * Sets dependencies
     * 
     * @param Zend_Db_Table_Abstract $gateway
     */
    public function __construct(Zend_Db_Table_Abstract $gateway)
    {
        $gateway->setRowClass('Application_Model_AdminUser');
        $this->gateway = $gateway;
    }
    
    /**
     * Insert new row into 'users' table
     * 
     * @param Application_Model_AdminUser $entity
     * @return Application_Model_UsersMapper
     */
    public function insert(Application_Model_AdminUser $entity)
    {
        $entity->setId(null);
        $data = $entity->toArray();
        $id = $this->gateway->insert($data);
        $entity->setId($id);
        return $this; 
    }
    
    /**
     * Updates row into 'users' table
     * 
     * @param Application_Model_AdminUser $entity
     * @return Application_Model_UsersMapper
     */
    public function updateProfile(Application_Model_AdminUser $entity)
    {
        $id = $entity->getId();
        if (empty($id)) {
            return $this;
        }
        $adapter = $this->gateway->getAdapter();
        $where = $adapter->quoteInto('id = ?', $id);
        $data = $entity->toArray();
        unset($data['password']);
        $this->gateway->update($data, $where);
        return $this;        
    }
    
    /**
     * Deletes row from 'users' table
     * 
     * @param Application_Model_AdminUser $entity
     * @return Application_Model_UsersMapper
     */
    public function delete(Application_Model_AdminUser $entity)
    {
        $id = $entity->getId();
        if (empty($id)) {
            return $this;
        }
        $adapter = $this->gateway->getAdapter();
        $where = $adapter->quoteInto('id = ?', $id);
        $this->gateway->delete($where);
        return $this; 
    }

    /**
     * Find user by Id
     *
     * @param int $id
     * @return null|Application_Model_AdminUser
     */
    public function findById($id)
    {
        $select = $this->gateway->select();
        $select->where('id = ?', $id, 'INTEGER'); // echo $select for debug!!!
        return $this->gateway->fetchRow($select);
        // return $this->gateway->fetchRow(['id = ?' => $id]); short version of above code
    }

    public function updatePass(Application_Model_AdminUser $entity)
    {
        $id = $entity->getId();
        if (empty($id)) {
            return $this;
        }

        $adapter = $this->gateway->getAdapter();

        $where = $adapter->quoteInto('id = ?', $id, 'INTEGER');
        $passwordHash = $adapter->quoteInto('SHA1(?)', $entity->getPassword());
        $data = ['password' => new Zend_Db_Expr($passwordHash)];

        $this->gateway->update($data, $where);
        return $this;
    }
}