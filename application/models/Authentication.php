<?php
/**
 * Glasses Shop ©
 * Copyright © 2012 Georgi Ivanov <gopa_bg@abv.bg>
 *
 * LICENSE
 *
 * A copy of this license is bundled with this package in the file LICENSE.txt.
 *
 * Copyright © Glasses Shop
 *
 * Platform that uses this site is protected by copyright.
 * It is provided solely for the use of this site and all its copying,
 * processing or use of parts thereof is prohibited and pursued by law.
 *
 * @author Georgi Ivanov <gopa_bg@abv.bg>
 * @category Application
 * @package Application Models
 * @copyright GAZE-ND
 */

/**
 * Login Form Authentication 
 *
 * @author Georgi Ivanov <gopa_bg@abv.bg>
 * @category Application
 * @package Application Models
 * @copyright GAZE-ND
 */
class Application_Model_Authentication
{

    public function login(Application_Form_Login $form)
    {
        // Get our authentication adapter and check credentials
        $adapter = $this->getAuthAdapter();
        
        $adapter->setIdentity($form->getValue('username'));
        
        $adapter->setCredential($form->getValue('password'));
        
        $auth = Zend_Auth::getInstance();
        
        $result = $auth->authenticate($adapter);
        
        if ($result->isValid()) {
            
            $user = $adapter->getResultRowObject();
            
            $auth->getStorage()->write($user);
            
            return true;
        }
        
        return false;
    }

    protected function getAuthAdapter()
    {
        $dbAdapter = Zend_Db_Table::getDefaultAdapter();
        
        $authAdapter = new Zend_Auth_Adapter_DbTable($dbAdapter);
        
        $authAdapter->setTableName('admin_users')
                    ->setIdentityColumn('username')
                    ->setCredentialColumn('password')
                    ->setCredentialTreatment('SHA1(?)');
        
        return $authAdapter;
    }
}
