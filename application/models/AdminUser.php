<?php
/**
 * Glasses Shop ©
 * Copyright © 2012 Georgi Ivanov <gopa_bg@abv.bg>
 *
 * LICENSE
 *
 * A copy of this license is bundled with this package in the file LICENSE.txt.
 *
 * Copyright © Glasses Shop
 *
 * Platform that uses this site is private by copyright.
 * It is provided solely for the use of this site and all its copying,
 * processing or use of parts thereof is prohibited and pursued by law.
 *
 * @author Georgi Ivanov <gopa_bg@abv.bg>
 * @category Application
 * @package Application Models
 * @copyright GAZE-ND
 */

/**
 * Admin User Entity class
 *
 * @author Georgi Ivanov <gopa_bg@abv.bg>
 * @category Application
 * @package Application Models
 * @copyright GAZE-ND
 */
class Application_Model_AdminUser extends GZend_Entity_AbstractEntity
{
    
    /**
     * User ID
     * 
     * @var integer
     */
    private $id;
    
    /**
     * Username
     *
     * @var string
     */
    private $username;
    
    /**
     * Password
     *
     * @var string
     */    
    private $password;
    
    /**
     * Email
     *
     * @var string
     */
    private $email;

    /**
     * First Name
     *
     * @var string
     */
    private $firstName;


    /**
     * Last Name
     *
     * @var string
     */
    private $lastName;

    /**
     * Constructor
     *
     * Sets entity's property values
     *
     * @param array $options
     */
    public function __construct(array $options = null)
    {
        if (null != $options) {
           if (isset($options['data'])) {
               $this->populate($options['data']);
           } else {
               $this->populate($options);
           }
        }
    }

    /**
     * Getter for $id
     *
     * @return number
     */
    public function getId()
    {
        return $this->id;
    }

	/**
     * Setter for $id
     *
     * @param number $id
     * @return Application_Model_AdminUser Provides fluent interface
     */
    public function setId($id)
    {
        $this->id = $id;
        return $this;
    }

	/**
     * Getter for $username
     *
     * @return string
     */
    public function getUsername()
    {
        return $this->username;
    }

	/**
     * Setter for $username
     *
     * @param string $username
     * @return Application_Model_AdminUser Provides fluent interface
     */
    public function setUsername($username)
    {
        $this->username = $username;
        return $this;
    }

	/**
     * Getter for $password
     *
     * @return string
     */
    public function getPassword()
    {
        return $this->password;
    }

	/**
     * Setter for $password
     *
     * @param string $password
     * @return Application_Model_AdminUser Provides fluent interface
     */
    public function setPassword($password)
    {
        $this->password = $password;
        return $this;
    }

	/**
     * Getter for $email
     *
     * @return string
     */
    public function getEmail()
    {
        return $this->email;
    }

	/**
     * Setter for $email
     *
     * @param string $email
     * @return Application_Model_AdminUser Provides fluent interface
     */
    public function setEmail($email)
    {
        $this->email = $email;
        return $this;
    }

    /**
     * Setter for $firstName
     *
     * @param string $firstName
     * @return Application_Model_AdminUser Provides fluent interface
     */
    public function setFirstName($firstName)
    {
        $this->firstName = $firstName;
        return $this;
    }

    /**
     * Getter for $firstName
     *
     * @return string
     */
    public function getFirstName()
    {
        return $this->firstName;
    }

    /**
     * Setter for $lastName
     *
     * @param string $lastName
     * @return Application_Model_AdminUser Provides fluent interface
     */
    public function setLastName($lastName)
    {
        $this->lastName = $lastName;
        return $this;
    }

    /**
     * Getter for $lastName
     *
     * @return string
     */
    public function getLastName()
    {
        return $this->lastName;
    }



}