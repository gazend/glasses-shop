<?php
class Application_Form_Login extends Zend_Form
{
    const PASSWORD_MIN_LENGTH = 5;

    private $inputDecorators = array(
        'ViewHelper',
        array('HtmlTag', array('class' => 'form-group'))
    );

    public function init()
    {
        $this->setMethod('post');

        //add username element
        $this->addElement('text', 'username', array(
            'label' => 'Username:',
            'filters' => array('StringTrim'),
            'required' => true,
            'class' => 'form-control',
            'decorators' => $this->inputDecorators
        ));

        //add element password
        $this->addElement('password', 'password', array(
            'label' => 'Password:',
            'required' => true,
            'class' => 'form-control',
            'validators' => array(
                array('StringLength', false, array('min' => self::PASSWORD_MIN_LENGTH))
            ),
            'decorators' => $this->inputDecorators
        ));


        $this->addDisplayGroup(
            array('username', 'password'),
            'credentials',
            array(
                'decorators' => array(
                    'formElements',
                    array('HtmlTag', array(
                        'class' => 'body bg-gray'
                    ))
                )
            )
        );

        // add element submit
        $this->addElement('submit', 'submit', array(
            'ignore' => true,
            'class' => 'btn bg-olive btn-block',
            'label' => 'Login',
            'decorators' => [
                'ViewHelper',
                ['HtmlTag', ['class' => 'footer']]
            ]
        ));
    }
}
