<?php
/**
 * Created by PhpStorm.
 * User: gopa
 * Date: 6/27/14
 * Time: 6:47 PM
 */

abstract class Application_Form_AbstractBootstrapable extends Zend_Form
{
    protected $defaultClass = 'form-control';

    protected $defaultElementDecorators = [
        'Label', 'ViewHelper', 'Description', 'Errors',
        [['form-group' => 'HtmlTag'], ['class' => 'form-group']],
        [['row' => 'HtmlTag'], ['class' => 'row']]
    ];

    protected $defaultFileDecorators = [
        'Label', 'File', 'Description', 'Errors',
        [['form-group' => 'HtmlTag'], ['class' => 'form-group']],
        [['row' => 'HtmlTag'], ['class' => 'row']]
    ];

    protected $defaultFormDecorators = [
        'FormElements',
        [['dl' => 'HtmlTag'], ['class' => 'col-md-12']],
        [['clear' => 'HtmlTag'], ['style' => 'clear: both', 'placement' => 'APPEND']],
        'Form'
    ];

    public function __construct($options = null)
    {
        $this->setDecorators($this->defaultFormDecorators)
             ->setElementDecorators($this->defaultElementDecorators);
        parent::__construct($options);
    }

    public function setWidthClass($widthClass)
    {
        $this->getDecorator('dl')->setOption('class', $widthClass);
    }

}