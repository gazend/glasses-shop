<?php

/**
 * Glasses Shop ©
 * Copyright © 2012 Georgi Ivanov <gopa_bg@abv.bg>
 *
 * LICENSE
 *
 * A copy of this license is bundled with this package in the file LICENSE.txt.
 *
 * Copyright © Glasses Shop
 *
 * Platform that uses this site is protected by copyright.
 * It is provided solely for the use of this site and all its copying,
 * processing or use of parts thereof is prohibited and pursued by law.
 *
 * @author Georgi Ivanov <gopa_bg@abv.bg>
 * @category Application
 * @package Application Plugins
 * @copyright GAZE-ND
 */

/**
 * Plugin for adding helper paths
 *
 * @author Georgi Ivanov <gopa_bg@abv.bg>
 * @category Application
 * @package Application Plugins
 * @copyright GAZE-ND
 */
class Application_Plugin_Helpers extends Zend_Controller_Plugin_Abstract
{

    /**
     * {@inheritdoc}
     *
     * @see Zend_Controller_Plugin_Abstract::preDispatch()
     */
    public function preDispatch(Zend_Controller_Request_Abstract $request)
    {
        /**
         * Get viewRenderer helper 
         */
        $view = Zend_Controller_Action_HelperBroker::getStaticHelper(
                'ViewRenderer')->view;
        $view->addHelperPath('GZend/View/Helper', 'GZend_View_Helper_');
    }
}