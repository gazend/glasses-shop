<?php
/**
 * Glasses Shop ©
 * Copyright © 2012 Georgi Ivanov <gopa_bg@abv.bg>
 *
 * LICENSE
 *
 * A copy of this license is bundled with this package in the file LICENSE.txt.
 *
 * Copyright © Glasses Shop
 *
 * Platform that uses this site is protected by copyright.
 * It is provided solely for the use of this site and all its copying,
 * processing or use of parts thereof is prohibited and pursued by law.
 *
 * @author Georgi Ivanov <gopa_bg@abv.bg>
 * @category Application
 * @package Application Plugins
 * @copyright GAZE-ND
 */

/**
 * Plugin for loading Layouts
 * 
 * Loads different Layout for the backend
 *
 * @author Georgi Ivanov <gopa_bg@abv.bg>
 * @category Application
 * @package Application Plugins
 * @copyright GAZE-ND
 */
class Application_Plugin_LayoutLoader extends Zend_Controller_Plugin_Abstract
{
    const ASSETS_FILE_CSS = 'css.php';
    const ASSETS_FILE_JS = 'js.php';
    
    /**
     * Controllers for the backend
     * @var string[]
     */
    protected $backendControllers = array('admin');
    
    /**
     * {@inheritdoc}
     * @see Zend_Controller_Plugin_Abstract::preDispatch()
     */
    public function preDispatch(Zend_Controller_Request_Abstract $request)
    {
        $controller = $request->getControllerName();
        if (in_array($controller, $this->backendControllers)) {
            $configPath = APPLICATION_PATH . '/layouts/assets/backend/';
        } else {
            $configPath = APPLICATION_PATH . '/layouts/assets/frontend/';
        }
        
        $view = Zend_Controller_Action_HelperBroker::getStaticHelper(
                'ViewRenderer')->view;
        
        $jsConfFile = $configPath . self::ASSETS_FILE_JS;
        $cssConfFile = $configPath . self::ASSETS_FILE_CSS;
        
        $view->headScript()->setSeparator("\n    ");
        $view->headLink()->setSeparator("\n    ");
        
        if (file_exists($jsConfFile)) {
            $scripts = include $jsConfFile;
            foreach ($scripts as $script) {
                $view->headScript()->appendFile($view->asset($script), null);
            }
        }
        
        if (file_exists($cssConfFile)) {
            $styles = include $cssConfFile;
            foreach ($styles as $style) {
                $view->headLink()->appendStylesheet($view->asset($style));
            }
        }
    }
}