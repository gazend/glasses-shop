<?php
return array(
    'css/bootstrap.min.css',
    'css/font-awesome.min.css',
    'css/ionicons.min.css',
    'css/morris/morris.css',
    'css/jvectormap/jquery-jvectormap-1.2.2.css',
    'css/fullcalendar/fullcalendar.css',
    'css/daterangepicker/daterangepicker-bs3.css',
    'css/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css',
    'css/AdminLTE.css'
);