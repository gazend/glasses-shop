<?php
/** 
 * Glasses Shop ©
 * Copyright © 2012 Georgi Ivanov <gopa_bg@abv.bg>
 *
 * LICENSE
 *
 * A copy of this license is bundled with this package in the file LICENSE.txt.
 *
 * Copyright © Glasses Shop
 *
 * Platform that uses this site is protected by copyright.
 * It is provided solely for the use of this site and all its copying,
 * processing or use of parts thereof is prohibited and pursued by law.
 * 
 * @author Georgi Ivanov <gopa_bg@abv.bg>
 * @category Application
 * @package Application Bootstrap
 * @copyright GAZE-ND
 */

/**
 * Application Bootstrap
 * 
 * @author Georgi Ivanov <gopa_bg@abv.bg>
 * @category Application
 * @package Application Bootstrap
 * @copyright GAZE-ND 
 */
class Bootstrap extends Zend_Application_Bootstrap_Bootstrap
{
	
	/**
	 * Initialize Application AutoLoader
	 * 
	 * @return Zend_Application_Module_Autoloader
	 */	
	protected function _initApplicationAutoloader()
	{
	    $autoloader = new Zend_Application_Module_Autoloader(array(
	            'namespace' => 'Application_',
	            'basePath' => APPLICATION_PATH 
	    ));
	    return $autoloader;
	} 
	
	/**
	 * Set the HTML DocType
	 */
	protected function _initDoctype()
	{
        $this->bootstrap('view');
        $view = $this->getResource('view');
        $view->doctype('HTML5');
	}
	
	/**
	 * Adding Application Routes
	 */
	protected function _initRoutes()
	{
	    $routesIniPath = __DIR__ . '/configs/routes.ini';
	    $config = new Zend_Config_Ini($routesIniPath);
        $router = Zend_Controller_Front::getInstance()->getRouter();
	    $router->addConfig($config, 'routes');
        foreach (new DirectoryIterator(APPLICATION_PATH . '/../modules') as $moduleDir) {
            /** @var DirectoryIterator $moduleDir */
            $configFile = $moduleDir->getPath() . '/'
                . $moduleDir->getFilename() . '/configs/routes.ini';
            if (file_exists($configFile)) {
                $router->addConfig(new Zend_Config_Ini($configFile), 'routes');
            }
        }
	}

    protected function _initDi()
    {
        $serviceManger = new GZend_Di_ServiceManager(APPLICATION_PATH . '/configs/di.php');
        foreach (new DirectoryIterator(APPLICATION_PATH . '/../modules') as $moduleDir) {
            /** @var DirectoryIterator $moduleDir */
            $configFile = $moduleDir->getPath() . '/'
                . $moduleDir->getFilename() . '/configs/di.php';
            if (file_exists($configFile)) {
                $serviceManger->loadFromFile($configFile);
            }
        }
        Zend_Registry::set('SERVICE_MANAGER', $serviceManger);
    }

    protected function _initNavigation()
    {
        $config = require_once APPLICATION_PATH . '/configs/navigation.php';
        $navigation = new Zend_Navigation($config);
        $this->bootstrap('view');
        /** @var Zend_View $view */
        $view = $this->getResource('view');
        $view->navigation($navigation);
    }

}

