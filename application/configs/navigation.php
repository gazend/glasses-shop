<?php
return [
    array(
        'label' => 'Categories',
        'action' => 'categories',
        'route' => 'admin_products',
        'pages' => array(
            array(
                'label' => 'New Category',
                'action' => 'new-category',
                'route' => 'admin_products',
                'class' => 'add-category-btn'
            )
        )
    ),
    array(
        'label' => 'Products',
        'action' => 'products',
        'route' => 'admin_products',
        'pages' => [
            array(
                'label' => 'New Product',
                'action' => 'new-product',
                'route' => 'admin_products'
            ),
            array(
                'label' => 'Product Properties',
                'action' => 'properties',
                'route' => 'admin_products'
            )
        ]
    )
];