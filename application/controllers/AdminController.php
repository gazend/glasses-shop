<?php
/**
 * Glasses Shop ©
 * Copyright © 2012 Georgi Ivanov <gopa_bg@abv.bg>
 *
 * LICENSE
 *
 * A copy of this license is bundled with this package in the file LICENSE.txt.
 *
 * Copyright © Glasses Shop
 *
 * Platform that uses this site is protected by copyright.
 * It is provided solely for the use of this site and all its copying,
 * processing or use of parts thereof is prohibited and pursued by law.
 *
 * @author Georgi Ivanov <gopa_bg@abv.bg>
 * @category Application
 * @package Application Controllers
 * @copyright GAZE-ND
 */

/**
 * Admin Controller
 *
 * @todo create profile action controller
 * @todo create profile form
 * @todo use the existing mapper to update data
 * @todo change session storage to database
 *
 * @author Georgi Ivanov <gopa_bg@abv.bg>
 * @category Application
 * @package Application Controllers
 * @copyright GAZE-ND
 */
class AdminController extends GZend_Controller_AdminActions
{
    const LOGIN_LAYOUT = 'login';
    
    public function init()
    {
        parent::init();
    }

    public function indexAction()
    {
        
    }
    
    public function loginAction()
    {
        Zend_Layout::getMvcInstance()->setLayout(self::LOGIN_LAYOUT);
        
        $request = $this->getRequest();
        $form = new Application_Form_Login();
        $this->view->form = $form;
        
        if ($this->getRequest()->isPost() && $form->isValid($request->getPost())) {
            $auth = new Application_Model_Authentication();
            if ($auth->login($form)) {
                $this->_helper->redirector->gotoSimpleAndExit('index', 'admin', 'default');
            }
        }
    }

    public function logoutAction()
    {
        Zend_Auth::getInstance()->clearIdentity();
        $this->_helper->redirector->gotoSimpleAndExit('login', 'admin', 'default');
    }


}

