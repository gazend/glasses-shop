<?php
$module = $_GET['module'];
$file = $_GET['file'];

$path = '../modules/' . $module . '/css/' . $file . '.css';

if (!file_exists($path)) {
    header("Status: 404 Not Found");
} else {
    header('Content-Type: text/css');
    include $path;
}
exit;