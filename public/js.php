<?php
$module = $_GET['module'];
$file = $_GET['file'];

$path = '../modules/' . $module . '/js/' . $file . '.js';

if (!file_exists($path)) {
    header("Status: 404 Not Found");
} else {
    header('Content-Type: text/javascript');
    include $path;
}
exit;