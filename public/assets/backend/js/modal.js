(function ($) {

    var modal = function (title, id) {

        if (!$('#' + id).length) {
            this.constructHtml(title, id);
        }

        this.element = $('#' + id);
    };

    modal.prototype.constructHtml = function (title, id) {
        $('body').append(
            '<div class="modal fade" id="'+ id +'"  ' +
                'tabindex="-1" role="dialog" aria-labelledby="myModalLabel" ' +
                'aria-hidden="true">' +
                '<div class="modal-dialog">' +
                    '<div class="modal-content">' +
                        '<div class="modal-header">' +
                            '<button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span>' +
                            '<span class="sr-only">Close</span>' +
                            '</button>' +
                            '<h4 class="modal-title" id="'+ id +'Label">'+ title +'</h4>' +
                        '</div>' +
                        '<div class="modal-body">' +
                            '...' +
                        '</div>' +
                        '<div class="modal-footer">' +
                            '<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>' +
                        '</div>' +
                    '</div>' +
                '</div>' +
            '</div>');
    };

    modal.prototype.show = function() {
        this.element.modal('show');
        return this;
    };

    modal.prototype.hide = function() {
        this.element.modal('hide');
        return this;
    };

    modal.prototype.setBodyContent = function(content) {
        // take the current element with class = modal-body
        $('.modal-body', this.element).html(content);
        return this;
    };

    modal.prototype.setTitle = function (title) {
        $('.modal-title', this.element).html(title);
        return this;
    };

    modal.prototype.appendFooterButton = function (label, btnClass, attributes, callBack) {
        var button = $('<button/>');

        button.html(label);

        if (typeof attributes != 'undefined' && typeof attributes != 'function') {
            $.each(attributes, function (key, value) {
                $(button).attr(key, value);
            });
        } else if (typeof attributes == 'function') {
            callBack = attributes;
        }

        var classes = 'btn ';
        if (typeof btnClass != 'undefined') {
            classes += btnClass;
        } else {
            classes += 'btn-default';
        }

        button.addClass(classes);

        if (typeof callBack != 'undefined') {
            button.on('click', callBack);
        }

        $('.modal-footer', this.element).append(button);

        return this;
    };

    window.Modal = modal;

})(window.jQuery);